(function addUsersFromLinkedinSearch() {
    "use strict"

    // The number of steps (not invitations)
    const MAX_NUMBER_OF_ACTIONS         = arguments[0] ?? 1

    // The max number of pages to go throught
    const MAX_NUMBER_OF_PAGES           = arguments[1] ?? 1

    // To send the message, or just to send empty invitations
    const EMPTY_INVITATION_MODE         = arguments[2] ?? true

    // No popups, no slack, no database
    const SIMPLE_MODE                   = arguments[3] ?? true

    // ------------------------------------------------------------------------------------------ //                                                                                                    // const SECOND_CONNECTIONS_ONLY       = true;
    // Some config constants and primary variables initialization                                 //                                                                                                    // const NOT_LOADED_PAGES_LIMIT        = 5;
    // ------------------------------------------------------------------------------------------ //
    
    const NUMBER_OF_ACTIONS_IN_BATCH    = 25;

    const THROTTLE_LIMIT                = 10;
    const EMAIL_BLOCK_LIMIT             = 10;
    const LATENT_THROTTLE_LIMIT         = 0.2;

    /*
     *
     */

    const MIN_AFTER_PAGECHANGE_DELAY    = 5000;   
    const MAX_AFTER_PAGECHANGE_DELAY    = 15000;

    const MIN_AFTER_CLICK_DELAY         = 1000;
    const MAX_AFTER_CLICK_DELAY         = 3000;

    // Not used at all if any of them is equal to 0.
    const MIN_DELAY_BETWEEN_CHUNKS      = 2500000;   
    const MAX_DELAY_BETWEEN_CHUNKS      = 5000000;

    const START_DELAY                   = 1000; 

    // Is also applied to any check-based data...
    const SCROLL_ITER_DELAY             = 1000;
    const SCROLL_ITER_PIXELS            = 250;
    const SCROLL_TIMES                  = 20;

    /*
     *
     */

    let   NAME                          = null;

    const MY_FULL_NAME                  = getMyName().getAttribute("alt");
    const MY_INITIALS                   = (function fullNameToInitials(fullName) {
                                               return fullName.charAt(0).toLowerCase() + 
                                                      fullName.charAt(fullName.indexOf(" ") + 1).toLowerCase() })(MY_FULL_NAME)

    const MY_NAME                       = MY_FULL_NAME.replace(/ .*/i, "");
    const MY_SURNAME                    = MY_FULL_NAME.replace(/[^ ]* /i, "");
    const MY_SHORT_NAME                 = ({
                                          // Waldemar              : "Waldo", // ?
                                             Zoriana               : "Zori",
                                          // Natali                : "Nata",
                                             Anastasia             : "Ana", // "Nastia",
                                          // Taline                : "Tali",
                                          // Nadia                 : "Nady",
                                          // Lesia                 : "Laci" 
                                          })[MY_NAME] ||
                                          MY_NAME; 

    const DATE_LETTER                   = ({ Mon: "a",
                                             Tue: "b",
                                             Wed: "c",
                                             Thu: "d",
                                             Fri: "e",
                                             Sat: "f",
                                             Sun: "g", })[(new Date()).toString().slice(0, 3)]

    const INVITATION_ID_LINK            = "https://go.hravity.com/" +
                                          MY_SURNAME.slice(0, 1).toLowerCase() + 
                                          MY_NAME.slice(0, 1).toLowerCase() + 
                                          DATE_LETTER.slice(0, 1);

    // Script removes {{NAME}} with a preceding space character, if there are initials or like that
    const INVITATION_MESSAGE            = "Hi {{NAME}}!\n\n" + 
                                          "It's {{MY_SHORT_NAME}} from yet another technical recruitment agency :) I invite you to connect so you could find me when there is an interest.\n\n" + 
                                          "I also encourage you to follow our LinkedIn page: {{INVITATION_ID_LINK}}\n" + 
                                          "We post updates from our Hiring FAQ for Engineers and Managers there.";

    /*
     *
     */

    const SLACK_SCRIPT_ID               = generateSlackScriptID();
    let   SLACK_THREAD_ID;
    const SLACK_TOKEN                   = "xoxb-19188155909-1321702106465-qarYOHyaHagQTK19bAemNOE4";
    const SLACK_CHANNEL                 = "C019A1UPVBN";

    /*
     *
     */

    const STRAPI_ENDPOINT               = "https://strapi-end.herokuapp.com"
    const STRAPI_TOKEN                  = "Nx6TiN1Zo8NqwBQ31pRaba72rsEPcqsH"
    const SCRIPT_LOG                    = {}

    /*
     *
     */

    // Filter or not people with keywords below.
    const FILTER_PEOPLE_MODE            = !EMPTY_INVITATION_MODE;

    // Excluding only in message mode.
    const EXCLUDE_WORDS                 = [ "developer", "engineer", "lead " /* not leader */, "specialist", "freelance", "trainee", "programmer",
                                            "artist", "designer", 
                                            "recruiter", "recruitment", "generalist", "hr", "talent",
                                            "trainer", "instructor", "student", 
                                            "market", "sale", "revenue", "financ", "expert", "consult",
                                            "open", "opportunity", "looking", "job" ];
                                            // advisor

                                            // Filtered in a wrong way
                                            // SW development Expertise

    // Those, who didn't accept connection, but was angry though || our customers.
    const EXCLUDE_PEOPLE                = [ "Ryan Dawson", "Serge Bukhar",
                                            "Angry Norw/Malasian @ Mary", "Sweden Job Site for IT @ Vovan" ];

                                            // 
    /*
     *
     */

    let pagesCounter                    = 0;
    let actionsCounter                  = 0;

    let invitationsSent                 = 0;
    let emptyInvitationsSent            = 0;

    let errorsEncountered               = 0;
    let invitationsThrottled            = 0;
    let latentlyThrottled               = 0;
    let invitationsBlocked              = 0;

    let initialConnections              = 0;
    let initialWithdrawals              = 0;
    let currentConnections              = 0;
    let currentWithdrawals              = 0;

    const REASON_TO_FINISH              = [ "Have accomplished all tasks",
                                            "Have passed all the pages", 
                                            "Have passed the last page possible for this search request",
                                            "Have encounered a critical error",

                                            "The search is broken" ];

    const SCRIPT_START_MOMENT           = (new Date()).toISOString()
    let SCRIPT_STATUS

    // ------------------------------------------------------------------------------------------ //
    // Logger, Errors, some text constants and primary variables                                  //
    // ------------------------------------------------------------------------------------------ //

    class ScriptError extends Error {                                                                                                                                                                   // Log to Slack parameter inside every mistake?
        constructor(code, ...params) {
            const errors                = { "moreThan300"           : { level: "ELEMENT",  message: "The invitation string is more than 300 symbols: " + params[0] },                                   // Send empty invitations insteas + IGNORE?
                                            "nameParseError"        : { level: "ELEMENT",  message: "Can't parse a name of a person: " + params[0] },
                                            "textareaInit"          : { level: "ELEMENT",  message: "Textarea can't be initiated for " + params[0] },
                                            "connectButtonDisabled" : { level: "ELEMENT",  message: "Can't open invitation window to " + params[0] },
                                            "emailBlock"            : { level: "ELEMENT",  message: "Invitation is blocked by email by " + params[0] },
                                            "throttle"              : { level: "ELEMENT",  message: "LinkedIn seems to throttle invitation to " + params[0] },

                                            "searchHalt"            : { level: "SCRIPT",   message: "Search page " + params[0] + " [LI: " + params[1] + "] can't be loaded" },
                                            "latentThrottling"      : { level: "SCRIPT",   message: params[0] + " of " + params[1] + " invitations " +
                                                                                                    "seem to be latently throttled by LinkedIn" },
                                            "invitationsLimit"      : { level: "SCRIPT",   message: "Invitations limit is reached " +
                                                                                                    "after " + params[0] + " [" + params[1] + "] actions" },
                                            "throttlingLimit"       : { level: "SCRIPT",   message: "Throttling limit of " + params[2] + " is reached " +
                                                                                                    "after " + params[0] + " [" + params[1] + "] actions" },
                                            "emailBlockLimit"       : { level: "SCRIPT",   message: "Email block limit of " + params[2] + " is reached " +
                                                                                                    "after " + params[0] + " [" + params[1] + "] actions" },

                                            "popupBlocker"          : { level: "BROWSER",  message: "Please, disable popup blocker" },
                                            "connectionSlack"       : { level: "BROWSER",  message: "Please, install Disable-Content-Security-Policy extension, " +
                                                                                                    "enable it, and refresh the page before starting the script" },
                                            "parseSlackId"          : { level: "BROWSER",  message: "Can't parse Slack ID" },

                                            // Ignoring interuption
                                            "filteringPerson"       : { level: "ELEMENT",  message: "Filtering " + params[0] + " from search results"},
                                            "pageLoad"              : { level: "PAGE",     message: "Can load only " + params[0] + " elements " +
                                                                                                    "on page " + params[1] + " [LI: " + params[2] + "]" },
                                            "slackMessageError"     : { level: "BROWSER",  message: "Can't send Slack message" }
                                          };

            super(errors[code]["message"]);

            this.name                   = "ScriptError";
            this.errorCode              = code;
            this.errorLevel             = errors[code]["level"];
        }
    }

    let scriptLog = (function() {
        let addNewLineFlag = false;

        function formatXX(number) {
            return ("0" + number).slice(-2);
        }

        function write(type, ...params) {
            console[type](...params)
            SCRIPT_LOG[getDateTimeNow()] = [ type, ...params ]
        }
    
        function getDateTimeNow() {
            let now = new Date();
            
            return formatXX(now.getFullYear()) + "/" +
                   formatXX(now.getMonth() + 1) + "/" +
                   formatXX(now.getDate()) + " " +
                   formatXX(now.getHours()) + ":" +
                   formatXX(now.getMinutes()) + ":" +
                   formatXX(now.getSeconds());
        }

        return function(type, ...params) {
            switch(type) {
                case "start":           scriptLog("clear");
                                        scriptLog("addTime", "Starting with " + params[0] + " conn's and " + params[1] + " with's");
                                        break;

                case "newPage":         scriptLog("twoLines", "Page #" + params[0] + " [LI: " + params[1] + "] with " + params[2] + "/" + params[3] + " (of " + params[4] + ") elements");
                                        break;  

                case "sleep":           scriptLog("twoLines", "Sleep " + params[0] + " sec (" + params[1] + " conn's & " + params[2] + " with's)");
                                        break;

                case "invitationSent":  scriptLog("addTimeLight", params[0] + ": Inv #" + params[1] + " to " + params[2]);
                                        break;

                case "emptyInvSent":    scriptLog("addTimeLight", params[0] + ": Empty inv #" + params[1] + " to " + params[2]);
                                        break;

                case "finish":          scriptLog("table", params[0]);
                                        break;

            // ---------------------

                case "addTime":         scriptLog("log", "%c " + getDateTimeNow() + " %c " + params[0] + " ", "background: gray; color: white;", "");
                                        break;

                case "addTimeLight":    scriptLog("log", "%c " + getDateTimeNow() + " %c " + params[0] + " ", "background: #ddd; color: white;", "");
                                        break;

                case "twoLines":        addNewLineFlag = true;
                                        scriptLog("addTime", ...params);
                                        addNewLineFlag = true;
                                        break;

                case "emptyLine":       if (addNewLineFlag === true) { addNewLineFlag = false;
                                                                       scriptLog("log", ""); }   
                                        break;

            // ---------------------
            
                case "info":
                case "warn":
                case "error":
                case "log": 
                case "table":           
                case "clear":
                default:                scriptLog("emptyLine");
                                        write(type, ...params);
                                        break;
            }
        }
    })();

    // ------------------------------------------------------------------------------------------ //
    // Getting HTML Elements                                                                      //
    // ------------------------------------------------------------------------------------------ //

    function getNextButton() {
        let nextButton = document.querySelector('button[aria-label="Next"]') ||
                         document.querySelector(".artdeco-pagination__button--next");

        if (nextButton === null ||
            nextButton.getAttribute("disabled") !== null) 
            return null;

        return nextButton;
    }

    function getSendInvitationButton() {
        return document.querySelector('button[aria-label="Send invitation"]') ||
               document.querySelector('button[aria-label="Send now"]') ||
               document.querySelector('button[aria-label="Done"]');
    }

    function getInvitationTextField() {
        return document.querySelector('textarea[id="custom-message"]') ||
               document.querySelector('.send-invite__custom-message');
    }

    function getAddANoteToInvitationButton() {
        return document.querySelector('button[aria-label="Add a note"]'); 
    }

    function getEmailConfirmField() {
        return document.querySelector('input[id="email"]');
    }
    
    function getCloseinvitationBoxButton() {
        return document.querySelector('button[aria-label="Dismiss"]'); 
    }
    
    function getCloseInvitationBoxButtons() {
        return document.querySelectorAll('button[aria-label="Dismiss"]'); 
    }

    function getLimitReachedGotItButton() {
        return document.querySelector('.ip-fuse-limit-alert__primary-action') || 
               document.querySelector('button[aria-label="Got it"]') || 
               document.querySelector('button[data-control-name="fuse_limit_got_it"]');
    }

    function getSearchResultsContainer() {
        return document.querySelector(".search-results__list");
    }

    /*
     * Search page elements
     */

    function getElementsOnPage() {
        return document.querySelectorAll(".actor-name");
    }

    function getAllButtons() {
        return document.querySelectorAll(".search-result__actions--primary");
    }

    function getNonDisabledButtons() {
        return [...getAllButtons()].filter(function (element) {
            return element.textContent.trim() === "Connect" &&
                   element.getAttribute("disabled") === null
        });
    }

    function getFilteredButtons() {
        return [...getNonDisabledButtons()].filter(function (element) {
            return filterPeople(element.getAttribute("aria-label"));
        });
    }

    /*
     * General script elements
     */

    function getMyName() { 
        return document.querySelector('.nav-item__profile-member-photo') ||
               document.querySelector('.global-nav__me-photo');
    }

    function getFavicon() {
        return document.querySelector("link[id='favicon-ico']") ||
               document.querySelector("link[rel='shortcut icon']");
    }

    /*
     * Elements from another window
     */

    function anotherWindowConnections(wind) {
        return wind.document.querySelector("h1[class='t-18 t-black t-normal']");
    }

    function anotherWindowWithdrawals(wind) {
        return wind.document.querySelector(".artdeco-pill__text");
    }

    // ------------------------------------------------------------------------------------------ //
    // Auxiliary logic elements                                                                   //
    // ------------------------------------------------------------------------------------------ //

    function getPersonNameFromButton(HTMLElement) {
        const NAME_PRESTRINGS = { "invitation": "Connect with ",
                                  "inviteSent": "Invite sent to " };
        let strWithName = HTMLElement.getAttribute("aria-label");

        for (let element in NAME_PRESTRINGS)
            if (strWithName.indexOf(NAME_PRESTRINGS[element]) !== -1)
                return removeNonLatinLetters(strWithName.slice(NAME_PRESTRINGS[element].length, strWithName.indexOf(".")));

        throw new ScriptError("nameParseError", strWithName);
    }

    function getMessageParsed(message, element) {
        NAME = getPersonFirstName(element);

        return message
               // If we can't parse the name properly (it's empty therefore), just saying "Hi" instead
               .replace(NAME ? "{{NAME}}" : " {{NAME}}",
                        NAME ? NAME : "")

               .replace("{{MY_NAME}}", MY_NAME)
               .replace("{{MY_SHORT_NAME}}", MY_SHORT_NAME)
               .replace("{{INVITATION_ID_LINK}}", INVITATION_ID_LINK);
    }

    function getPersonFirstName(HTMLelement) {
        let fullName = getPersonNameFromButton(HTMLelement);

        let firstName;
        if (fullName.indexOf(" ") === -1) firstName = fullName;
        else firstName = fullName.slice(0, fullName.indexOf(" "));

        // Getting rid of a bias with initials, mr., dr., and so on
        if (firstName.length <= 2) firstName = "";

        firstName = capitalizeFirstLetter(firstName);

        // Handling double names like Nina-Maria
        if (firstName.indexOf("-") !== -1) firstName = firstName.slice(0, firstName.indexOf("-") + 1) 
                                                     + capitalizeFirstLetter(firstName.slice(firstName.indexOf("-") + 1));

        return firstName;
    }

    function cutStringTo300(string) {                                                                                                                                                                   // Show a mistake and send an empty invitation instead
        if (string.length > 300)
            // This time it was decided not to cut, but to throw an error
            throw new ScriptError("moreThan300", "[" + string.length + "] " + string);

        return string.slice(0, 300);
    }

    function capitalizeFirstLetter(str) {
        if (str.toUpperCase() === str ||
            str.toLowerCase() === str)
            return str.charAt(0).toUpperCase() + str.toLowerCase().slice(1);
            
        return str;
    }

    function removeNonLatinLetters(str) {
        let charset = "a-zA-Z" + 
                      "0-9" +
                      "ÀÁÂÃÄÅÆÇŒÈÉÊËÌÍÎÏàáâãäåæçœèéêëìíîïÐÑÒÓÔÕÖØÙÚÛÜÝŸÞẞðñòóôõöøùúûüýÿþß" + 
                      // For double names and surnames.
                      "-";
        let regex = new RegExp(`[^${charset}\\s]`, "g");
        return str.replace(regex, "").trim();
    }

    function startFlashingFavicon() {
        return setInterval((a, b) => { if (getFavicon().getAttribute("href") == a)
                                           getFavicon().setAttribute("href", b);
                                       else
                                           getFavicon().setAttribute("href", a); },
                           500,
                           getFavicon().getAttribute("href"),
                           // "https://d3ki9tyy5l5ruj.cloudfront.net/obj/8897db9ac54fb68c74f76b4d3e2e753b524fca42/Favicon@3x.png"
                           // "https://cdn.sstatic.net/Sites/askubuntu/Img/favicon.ico"
                           "https://icons.iconarchive.com/icons/icons8/ios7/512/Sports-Finish-Flag-icon.png");
    }

    async function getConnections() {                                                                                                                                                                   // Search for the way to load the data via XHR without a new window
        if (SIMPLE_MODE === true) return "0";

        let win = window.open("https://www.linkedin.com/mynetwork/invite-connect/connections/");
        if (win === null) throw new ScriptError("popupBlocker");

        let result = await getWhenReady(() => anotherWindowConnections(win));
        win.close();

        if (!result) return "0";

        return parseInt(result.innerText.trim().slice(0, result.innerText.trim().indexOf(" ")).replace(",", ""));
    }

    async function getWithdrawals() {                                                                                                                                                                   // Search for the way to load the data via XHR without a new window
        if (SIMPLE_MODE === true) return "0";

        let win = window.open("https://www.linkedin.com/mynetwork/invitation-manager/sent/");
        if (win === null) throw new ScriptError("popupBlocker");

        let result = await getWhenReady(() => anotherWindowWithdrawals(win));
        win.close();

        if (!result) return "0";

        return parseInt(result.innerText.trim().slice(8, -1).replace(",", ""));
    }

    function filterPeople(buttonString) {
        if (!FILTER_PEOPLE_MODE) return true;

        for (let element of EXCLUDE_WORDS.concat(EXCLUDE_PEOPLE))
            if (buttonString.toLowerCase().indexOf(element.toLowerCase()) !== -1)
                return false

        return true;
    }

    function getFinalTable() {
        return { "Number of interactions":            { start: MAX_NUMBER_OF_ACTIONS, finish: actionsCounter },
                 "Invitations sent":                  { start: "-", finish: invitationsSent },
                 "Empty invitations sent":            { start: "-", finish: emptyInvitationsSent },
                 "":                                  {},
                 "Connections":                       { start: initialConnections, finish: currentConnections },
                 "Withdrawals":                       { start: initialWithdrawals, finish: currentWithdrawals },
                 "Latently throttled":                { start: "-", finish: latentlyThrottled },
                 " ":                                 {},
                 "Invitations throttled":             { start: "-", finish: invitationsThrottled },
                 "Invitations blocked":               { start: "-", finish: invitationsBlocked },
                 "Other exceptions":                  { start: "-", finish: errorsEncountered - invitationsThrottled - invitationsBlocked } }
    }

    /*
     * Slack functions
     */

    function generateSlackScriptID(hashLength = 16, characters = "0123456789ABCDEF") {
        return [...Array(hashLength).keys()].map(() => characters.charAt(Math.floor(Math.random() * characters.length))).join("");
    }

    async function sendSlackMessage(text) {
        if (SIMPLE_MODE === true) return null;

        let address = "https://slack.com/api/chat.postMessage" 
                    + "?token=" + SLACK_TOKEN
                    + "&channel=" + SLACK_CHANNEL
                    + (SLACK_THREAD_ID ? "&thread_ts=" + SLACK_THREAD_ID : "")
                    + '&text=' + encodeURI(text);

        return new Promise((resolve, reject) => { let channel = new XMLHttpRequest();
                                                  channel.open("GET", address);
                                                  channel.onerror = () => { reject(new ScriptError("connectionSlack")); };
                                                  channel.onload = () => { resolve(channel.responseText); };
                                                  channel.send(""); })

                   .then((result)            => { if (SLACK_THREAD_ID) return;
                                                  let res_parsed = JSON.parse(result);
                                                  if (!res_parsed.message.ts) throw new ScriptError("parseSlackId");
                                                  SLACK_THREAD_ID = res_parsed.message.ts; });
    }

    async function sendSlackThreadMessage(text) {
        try {
            sendSlackMessage(text);
        } catch(err) {
            errorsEncountered++;
            scriptLog("log", (new ScriptError("slackMessageError")).message);
        }
    }

    function getInitialSlackMessage() {
        return "*[ Invitations " + SLACK_SCRIPT_ID + " ]*" + "\n" + 
               "\n" +
               "* " + MY_FULL_NAME + "\n" +
               "* " + initialConnections + " conn's and " + initialWithdrawals + " with's" + "\n" + 
               "* " + "Reach " + MAX_NUMBER_OF_ACTIONS + (EMPTY_INVITATION_MODE ? " empty" : "") + " invitations in " + MAX_NUMBER_OF_PAGES + " pages" + "\n" +
               "* " + "LinkedIn page: " + getLinkedInPage() + "\n" +
               "* " + "Keywords: " + getKeywords() + "\n" +
               "* " + "Region: " + getRegion() + "\n" +
               "* " + "Network: " + getNetwork() + "\n" +
               "* " + "Industries: " + getIndustry();
    }

    function getFinalSlackMessage() {
        return "*[ " + SLACK_SCRIPT_ID + " ]* is finished:" + "\n" +
               "\n" +
               "* " + initialConnections + " => " + currentConnections + " connections" + "\n" +
               "* " + initialWithdrawals + " => " + currentWithdrawals + " withdrawals" + "\n " + 
               "* " + (EMPTY_INVITATION_MODE ? emptyInvitationsSent : invitationsSent) + " (of " + actionsCounter + ")" + 
                      (EMPTY_INVITATION_MODE ? " empty" : "") + " inv sent (" + MAX_NUMBER_OF_ACTIONS + " plan)" + "\n" +
               "* " + pagesCounter + " pages passed of " + MAX_NUMBER_OF_PAGES + " planned" + "\n" +
               "* " + "Err: thr " + invitationsBlocked + 
                        " / lat thr " + latentlyThrottled + 
                        " / blk " + invitationsBlocked + 
                        " / oth " + (errorsEncountered - invitationsThrottled - invitationsBlocked);
    }

    /*
     * 
     */

    function postScriptInfoToDatabase() {
        if (SIMPLE_MODE === true) return null

        SCRIPT_STATUS = "normal_finish"

        return fetch(STRAPI_ENDPOINT + "/linalytics-scripts-results?token=" + STRAPI_TOKEN,
                     { method: "POST", 
                       headers: { "Content-Type": "application/json" }, 
                       body: JSON.stringify( { initials: MY_INITIALS,
                                               actions: actionsCounter,
                                               invitations: invitationsSent + emptyInvitationsSent,
                                               is_empty: EMPTY_INVITATION_MODE,
                                               connections: initialConnections,
                                               withdrawals: initialWithdrawals,
                                               datetime: SCRIPT_START_MOMENT,
                                               connections_end: currentConnections,
                                               withdrawals_end: currentWithdrawals,
                                               datetime_end: (new Date()).toISOString(),
                                               status: SCRIPT_STATUS,
                                               log: SCRIPT_LOG } ) } )
               .then(response => response.json())
               .then(result => console.log(result))
    }

    /*
     * LinkedIn address parsing functions
     */

    function getURIParam(param) {
        param = param.replace(/[\[]/g, '\\[').replace(/[\]]/g, '\\]');
        let regex = new RegExp('[\\?&]' + param + '=([^&#]*)');
        let results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    function parseLinkedInParametres(variable, dataArray) {
        return getURIParam(variable).replace(/[\[\]"]/gi, "")
                                    .split(",")
                                    .map((element) => { if (dataArray[element]) return dataArray[element]; else return "[" + element + "]"})
                                    .join(", ");
    }

    function getLinkedInPage() {
        return getURIParam("page") || 1;
    }

    function getKeywords() {
        return getURIParam("keywords");
    }

    function getNetwork() {
        return parseLinkedInParametres("facetNetwork", 
                                       { "F": "1st",
                                         "S": "2nd",
                                         "O": "3d+" });
    }

    function getRegion() {
        return getURIParam("facetGeoRegion")
             ? parseLinkedInParametres("facetGeoRegion", 
                                       { "us:704"       : "USA // Greater St. Louis Area",
                                         "us:84"        : "USA/SFBA" })
             : parseLinkedInParametres("facetGeoUrn", 
                                       { "90000084"     : "USA/SFBA",
                                         "90000704"     : "Greater St. Louis",
                                         "102264497"    : "Ukraine" });
                                       // 90000091 Seattle
    }

    function getIndustry() {
        return parseLinkedInParametres("facetIndustry", 
                                       { "3": "C Hard",
                                         "4": "C Soft",
                                         "5": "C Netw",
                                         "96": "I T and Serv",
                                         "109": "C Gam",
                                         "118": "C n N Sec" });
    }
     
    // ------------------------------------------------------------------------------------------ //
    // Browser actions functions                                                                  //
    // ------------------------------------------------------------------------------------------ //

    async function sleepUntilNextBatch(connections, withdrawals) {
        let interBatchDelay = getBatchDelay();
        await actionWithDelay(() => scriptLog("sleep", 
                                              Math.floor(interBatchDelay/1000),
                                              connections,
                                              withdrawals),
                              0, 
                              interBatchDelay);
    }
    
    async function addInvitationMessage(str) {
        let elem = getInvitationTextField();
        await actionWithDelay(() => {
            elem.value = str;

            let theEvent = new Event('input', { 'bubbles': true, 'cancelable': true });
            elem.dispatchEvent(theEvent);
        }, 0, getAfterClickDelay());
    }

    async function closeInvitationWindow(cross_button) {
        await singleClick(cross_button);
    }

    async function closeAllInvitationWindows() {
        if ([...getCloseInvitationBoxButtons()].length === 0)
            return false;

        for (let elem of [...getCloseInvitationBoxButtons()]) 
            await closeInvitationWindow(elem);
    }

    async function scrollIteration() {
        await actionWithDelay(() => window.scrollTo(0, 0), 0, SCROLL_ITER_DELAY);

        for (let scrollCounter = 0; scrollCounter < SCROLL_TIMES; scrollCounter++) {
            await actionWithDelay(() => { window.focus();
                                          window.scrollBy(0, SCROLL_ITER_PIXELS); },
                                  0, 
                                  SCROLL_ITER_DELAY);
            if (getElementsOnPage().length === 10) return true;
        }

        return null;
    }

    async function scrollAndLoadWholePage() {                                                                                                                                                           // Find the way to understand 
        await getWhenReady(scrollIteration);

        if (getElementsOnPage().length < 10 && getNextButton())
            try { throw new ScriptError("pageLoad", getElementsOnPage().length, pagesCounter, getLinkedInPage()); } 
            catch(err) { errorsEncountered++;
                         scriptLog("warn", err.name + ". " + err.message);
                         await sendSlackThreadMessage("*[ " + SLACK_SCRIPT_ID + " ]* " + err.name + ". " + err.message); }
    }

    async function actionWithDelay(action, delayBefore = 0, delayAfter = getAfterClickDelay()) {
    
        async function bacTimeout(func, delay, ...params) {

            // Creating a worker by passing a function to it.
            function createWorker(func) {
                let blobURL = URL.createObjectURL(new Blob([ '(',
                                                             func,
                                                             ')()' ], 
                                                  { type: 'application/javascript' } ));
                let worker = new Worker(blobURL);
                // URL.revokeObjectURL(blobURL);
                return worker;
            }
    
            // Creating a worker, which will manage a timeout.
            function wrkTimeout(delay) {
                let worker = createWorker(function() { setTimeout(() => postMessage("EOW"), delay) }.toString().replace("delay", delay));
    
                return new Promise(resolve => { 
                    worker.onmessage = (msg) => {
                        worker.terminate();
                        resolve();
                    }
                });
            }
    
            // Creating a standard timer.
            function oldTimeout(delay) {
                return new Promise( resolve =>
                    setTimeout( () => resolve(), delay )
                );
            }
    
            let olschoolTimeout = oldTimeout(delay);
            let workerTimeout   = wrkTimeout(delay);
    
            return new Promise(resolve => { let mutexed = false;
                                            let fu = async () => { if (mutexed === true) return;
                                                                   mutexed = true;
                                                                   await func(...params); 
                                                                   resolve(); }
                                
                                            workerTimeout.then(fu); 
                                            olschoolTimeout.then(fu); });
        }

        return new Promise (resolve => bacTimeout(() => { action();
                                                          resolve(); }, 
                                                  delayBefore))
                   .then(() => new Promise(resolve => bacTimeout(() => resolve(), delayAfter)));
    }

    async function getWhenReady(check, maxDelay = SCROLL_ITER_DELAY * SCROLL_TIMES) {
        let result;
        for (let i = 0; i <= maxDelay; i += SCROLL_ITER_DELAY) {
            result = await check();
            if (result === null || result === undefined)
                await actionWithDelay(() => {}, 0, SCROLL_ITER_DELAY);
            else
                return result;
        }
        return null;
    }

    async function singleClick(element, ...params) {
        return actionWithDelay(element.click.bind(element), ...params)
    }

    async function goToNextPage() {
        return singleClick(getNextButton(), 0, getAfterPagechangeDelay())    
    }

    function getRandomDelay(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function getAfterClickDelay() {
        return getRandomDelay(MIN_AFTER_CLICK_DELAY, MAX_AFTER_CLICK_DELAY);
    }

    function getAfterPagechangeDelay() {
        return getRandomDelay(MIN_AFTER_PAGECHANGE_DELAY, MAX_AFTER_PAGECHANGE_DELAY);
    }

    function getBatchDelay() {
        return getRandomDelay(MIN_DELAY_BETWEEN_CHUNKS, MAX_DELAY_BETWEEN_CHUNKS);
    }

    // ------------------------------------------------------------------------------------------ //
    // Four primary logic functions                                                               //
    // ------------------------------------------------------------------------------------------ //

    async function invitePerson(connect_button) {       
        if (!filterPeople(connect_button.getAttribute("aria-label"))) {
            scriptLog("log", (new ScriptError("filteringPerson", "`" + connect_button.getAttribute("aria-label") + "`")).message);
            return;
        }
        
        await singleClick(connect_button);

        // Sometimes, connect buttons are not clickable and the modal window just doesn't open
        if (!getSendInvitationButton() ||
            !getCloseinvitationBoxButton())
            throw new ScriptError ("connectButtonDisabled", getPersonNameFromButton(connect_button));

        // If LinkedIn asks email - just close the modal window and continue with the next one
        if (getEmailConfirmField())
            throw new ScriptError ("emailBlock", getPersonNameFromButton(connect_button));

        if (!EMPTY_INVITATION_MODE) {
            // If there is a modal window with "add a note" button - click it and make the textarea visible
            if (getAddANoteToInvitationButton())
                await singleClick(getAddANoteToInvitationButton());                                   
            
            // Just expecting any kind of mistakes concerned to textarea value assignment, 'cause they always appear here
            if (getInvitationTextField() === null) 
                throw new ScriptError("textareaInit", getPersonNameFromButton(connect_button));
            
            await addInvitationMessage(cutStringTo300(getMessageParsed(INVITATION_MESSAGE, connect_button)));
        }

        // The check is done on the very beginning;
        await singleClick(getSendInvitationButton());

        // A window doesn't want to close - throttling usually starts here. We should stop with error. 
        if (getSendInvitationButton())
            throw new ScriptError ("throttle", getPersonNameFromButton(connect_button));

        // Check if we have reached the limit
        if (getLimitReachedGotItButton())
            throw new ScriptError ("invitationsLimit", actionsCounter, invitationsSent + emptyInvitationsSent);

        if (EMPTY_INVITATION_MODE) {
            emptyInvitationsSent++;
            scriptLog("emptyInvSent", actionsCounter, emptyInvitationsSent, getPersonNameFromButton(connect_button));
        } else {  
            invitationsSent++;
            scriptLog("invitationSent", actionsCounter, invitationsSent, getPersonNameFromButton(connect_button));
        }
    }

    async function walkThroughPage() {
        // Sometimes search page can't be loaded due to internal LinkedIn reasons. 
        if (getSearchResultsContainer() === null) throw new ScriptError("searchHalt", pagesCounter, getLinkedInPage());

        await scrollAndLoadWholePage();
        scriptLog("newPage", pagesCounter, getLinkedInPage(), getFilteredButtons().length, getNonDisabledButtons().length, getElementsOnPage().length);

        let buttons = getNonDisabledButtons();
        for (let i = 0; i < buttons.length && actionsCounter < MAX_NUMBER_OF_ACTIONS; i++) {
            actionsCounter++; 

            try {
                await closeAllInvitationWindows();
                await invitePerson(buttons[i]);
            } catch(err) {
                if (err.name !== "ScriptError" ||
                    err.errorLevel !== "ELEMENT") 
                    throw err; 

                errorsEncountered++;
                scriptLog("warn", err.name + ". " + err.message);

                if (err.errorCode === "throttle") {
                    invitationsThrottled++;
                    if (invitationsThrottled >= THROTTLE_LIMIT)
                        throw new ScriptError("throttlingLimit", actionsCounter, invitationsSent + emptyInvitationsSent, THROTTLE_LIMIT);
                }

                if (err.errorCode === "emailBlock") {
                    invitationsBlocked++;
                    if (invitationsBlocked >= EMAIL_BLOCK_LIMIT)
                        throw new ScriptError("emailBlockLimit", actionsCounter, invitationsSent + emptyInvitationsSent, EMAIL_BLOCK_LIMIT);
                }
            } finally {
                await closeAllInvitationWindows();
            }

            // Splitting the flow of actions to the batches of specific size
            if ((function isItATimeToSleep() { return !(function isSleepIgnored() {        return MAX_DELAY_BETWEEN_CHUNKS === 0 || 
                                                                                                  MIN_DELAY_BETWEEN_CHUNKS === 0 })() &&
                                                       (function hasTimeCome() {           return actionsCounter % NUMBER_OF_ACTIONS_IN_BATCH === 0; })() &&
                                                       (function isThereMoreMork() {       return actionsCounter < MAX_NUMBER_OF_ACTIONS; })() &&
                                                       (function isNotLastPageFinished() { return pagesCounter < MAX_NUMBER_OF_PAGES &&
                                                                                                  getNextButton() ||
                                                                                                  i < buttons.length - 1; })() })()) {
                currentConnections = await getConnections();
                currentWithdrawals = await getWithdrawals();
                latentlyThrottled = (invitationsSent + emptyInvitationsSent)
                                  - (currentConnections - initialConnections)
                                  - (currentWithdrawals - initialWithdrawals);
                
                if (latentlyThrottled >= LATENT_THROTTLE_LIMIT * (invitationsSent + emptyInvitationsSent))
                    throw new ScriptError("latentThrottling", latentlyThrottled, invitationsSent + emptyInvitationsSent);

                await sleepUntilNextBatch(currentConnections, currentWithdrawals);
            }
        }
    }

    async function init() {
        do {
            pagesCounter++;
            if (pagesCounter > 1 && getNextButton())
                await goToNextPage();

            try {
                await walkThroughPage();
            } catch(err) {
                if (err.name !== "ScriptError" ||
                    err.errorLevel !== "PAGE") 
                    throw err;

                errorsEncountered++;
                scriptLog("warn", err.name + ". " + err.message);                                                                                                                                       // Make this mistake of two rows (?)
            }

        } while (pagesCounter < MAX_NUMBER_OF_PAGES &&
                 actionsCounter < MAX_NUMBER_OF_ACTIONS &&
                 getNextButton());
    }

    // ------------------------------------------------------------------------------------------ //
    // Well, the code of a function                                                               //
    // ------------------------------------------------------------------------------------------ //

    // Initializing the function asynchrounously
    return new Promise(resolve => setTimeout(() => resolve(), 
                                             START_DELAY))
               .then(async ()      => { initialConnections = await getConnections();
                                        initialWithdrawals = await getWithdrawals();
                                        scriptLog("start", initialConnections, initialWithdrawals); 
                                        await sendSlackMessage(getInitialSlackMessage()); })
               .then(async ()      => { await init() })
               .catch(async (err)  => { if (err.name !== "ScriptError" ||
                                            err.errorLevel !== "SCRIPT") 
                                            throw err;

                                        errorsEncountered++;
                                        scriptLog("error", err.name + ". " + err.message);
                                        await sendSlackThreadMessage("*[ " + SLACK_SCRIPT_ID + " ]* " + err.name + ". " + err.message); })
               .then(async ()      => { currentConnections = await getConnections();
                                        currentWithdrawals = await getWithdrawals();
                                        latentlyThrottled = (invitationsSent + emptyInvitationsSent)
                                                          - (currentConnections - initialConnections)
                                                          - (currentWithdrawals - initialWithdrawals);

                                        scriptLog("finish", getFinalTable());
                                        await sendSlackThreadMessage(getFinalSlackMessage());
                                        await postScriptInfoToDatabase()
                                        /* console.log(SCRIPT_LOG) */ })
               .then(()            => { startFlashingFavicon(); })
               .catch((err)        => { scriptLog("error", err.name + ". " + err.message); });
})(null, null, null, false);