(function addUsersFromLinkedinSearch() {
    "use strict"

    // Working modes
    const EMPTY_INVITATION_MODE         = false; // Send empty invitations, invitation text is disabled
    const NO_MESSAGE_MODE               = false; // Do not send 3d level messages to 3d level connections

    // The first encountered works.
    const NUMBER_OF_PAGES_TO_GO_TROUGH  = 20;
    const NUMBER_OF_PEOPLE_TO_TRY       = 200;

    // Sleep after number of actions
    const BUNCH_SIZE                    = 40; 

    // Defining delays for clicking, page changing, scrolling, bunching and start
    const MIN_AFTER_CLICK_DELAY         = 1000;
    const MAX_AFTER_CLICK_DELAY         = 3000;

    const MIN_AFTER_PAGECHANGE_DELAY    = 5000;   
    const MAX_AFTER_PAGECHANGE_DELAY    = 15000;

    const MIN_BUNCH_DELAY               = 3600000;   
    const MAX_BUNCH_DELAY               = 7200000;

    const START_DELAY                   = 1000; 

    const INMAIL_DELAY                  = 2592000000; // One month in ms

    const SCROLL_DELAY                  = 10000;
    const SCROLL_BY                     = 100000; // In pixels 

    // Script removes {{NAME}} with a preceding space character, if there are initials or like that
    let   NAME                          = null; // "{{NAME}}"
    const MY_NAME                       = getMyName(); // "{{MY_NAME}}"
    const MY_SHORT_NAME                 = ( { Valdemar              : "Valdo",
                                              Zoriana               : "Zori",
                                              Natali                : "Nata",
                                              Anastasia             : "Ana" } )[MY_NAME] ||
                                          MY_NAME; 

    const INVITATION_ID_LINK            = "https://go.hravity.com/abb";
    const INVITATION_MESSAGE            = "Hi {{NAME}}!\n\n" + 
                                          "This is {{MY_SHORT_NAME}} from yet another technical recruitment agency :) I invite you to connect so you could find me when there is an interest.\n\n" + 
                                          "I also encourage you to follow our LinkedIn page: {{INVITATION_ID_LINK}}\n" + 
                                          "We post updates there from our Hiring FAQ for Engineers and Managers."
                         
    const INMAIL_ID_LINK                = "https://go.hravity.com/linkedin";
    const INMAIL_MESSAGE                = "<p>Hi {{NAME}},</p>" +
                                          "<p><br></p>" +
                                          "<p>I invite you to follow our LinkedIn page: {{INMAIL_ID_LINK}}</p>" +
                                          "<p><br></p>" +
                                          "<p>This is a hiring FAQ. Without magic, in simple words and with lots of details, we’ll disclose the most commonplace mistakes, right practices and the roots of HR technology and science around.</p>" +
                                          "<p><br></p>" +
                                          "<p>Hiring is the most magic-minded area. As enginners and managers we want it to have more of science and technology. For the last 10 years we’ve been researching and developing a versatile approach to it. And a brief time ago we’ve started sharing our knowledge and vision.</p>" +
                                          "<p><br></p>" +
                                          "<p>Message us if there is a question you’d like to get the answer or read about.</p>" +
                                          "<p><br></p>" +
                                          "<p>Thank you and welcome :)";

    // ------------------------------------------------------------------------------------------ //
    // Logger, Errors, some text constants and primary variables                                  //
    // ------------------------------------------------------------------------------------------ //

    let pagesCounter                    = 0;
    let actionsCounter                  = 0;

    let invitationsSent                 = 0; // successful invitations
    let emptyInvitationsSent            = 0; // successful empty invitatinos
    let messagesSent                    = 0; // those from the 3d level to whom messages for free were sent
    let messagesPassed                  = 0;
    let errorsEncountered               = 0;

    /*
     *
     */

    const BUTTON_NAME_PRESTRINGS        = { "invitation"            : "Connect with ",
                                            "inviteSent"            : "Invite sent to ",

                                            "inMail"                : "Send InMail to ",
                                            "message"               : "Send message to " };

    const BUTTON_TEXTS                  = { "connectButton"         : "Connect",
                                            "messageButton"         : "Message" };

    const INMAIL_STATUS_STRINGS         = { "freeInMail"            : "Free message" };

    /*
     *
     */

    class ScriptError extends Error {
        constructor(code, param) {
            let errors                  = { "wrongButton"           : "The action for button {{PARAM}} is not defined",
                                            "messageButtonDisabled" : "Message window to {{PARAM}} can't be opened",
                                            "textareaInit"          : "Textarea can't be initiated for {{PARAM}}",
                                            "connectButtonDisabled" : "Can't open invitation window to {{PARAM}}",
                                            "wrongAction"           : "Action can't be executed: {{PARAM}}",
                                            "nameParseError"        : "Can't parse a name of a person: {{PARAM}}",
                                            "dateParse"             : "Can't parse a date: {{PARAM}}", 
                                            "moreThan300"           : "The invitation string is more than 300 symbols: {{PARAM}}" };

            let message = errors[code].replace("{{PARAM}}", param);
            super(message);
            this.name = "ScriptError";
        }
    }

    class LinkedInError extends Error {
        constructor(code, param) {
            let errors                  = { "invitationsLimit"      : "Invitations limit is reached after {{PARAM}} actions",
                                            "messagesLimit"         : "Messages limit is reached after {{PARAM}} actions",
                                            "emailBlock"            : "Invitation is blocked by email by {{PARAM}}",
                                            "messageDuplicate"      : "This message was sent before to {{PARAM}}",
                                            "notAccepted"           : "Previous message was not accepted by {{PARAM}}",
                                            "wrongAdressee"         : "Trying to send message to 1st level connection: {{PARAM}}",
                                            "billedInMail"          : "Trying to send billed message to {{PARAM}}",
                                            "wait30Days"            : "Sent something else to {{PARAM}} withing the last 30 days" };

            let message = errors[code].replace("{{PARAM}}", param);
            super(message);
            this.name = "LinkedInError";
        }
    }

    function scriptLog(type, param) {
        switch(type) {
            case "start":
                console.clear();
                scriptLog("addTime", "So, let's start our workout :)");
                break;

            case "newPage":
                console.log("");
                scriptLog("addTime", "Starting the page #" + pagesCounter + " [LI: " + (getURIParam("page") || 1) + "] with " + param + " elements");
                console.log("");
                break;

            case "sleep":
                console.log("");
                scriptLog("addTime", "Going to sleep for " + param + " seconds");
                console.log("");
                break;

            case "finish":        
                console.table( { "People interacted" : actionsCounter,
                                 "Invitations sent" : invitationsSent,
                                 "Empty invitations sent" : emptyInvitationsSent,
                                 "Messages sent" : messagesSent,
                                 "Messages passed" : messagesPassed,
                                 "Errors encountered" : errorsEncountered },
                              "table");
                break;

            case "invitationSent":
                console.log(actionsCounter + ": Invitation #" + invitationsSent + " is sent to " + param);
                break;

            case "emptyInvitationSent":
                console.log(actionsCounter + ": Empty invitation #" + emptyInvitationsSent + " is sent to " + param);
                break;

            case "messageSent":
                console.log(actionsCounter + ": A message to a person #" + messagesSent + " is sent - " + param);
                break;

            case "messagePassed":
                console.log(actionsCounter + ": A message to a person #" + messagesPassed + " is passed - " + param);
                break;

            case "warn":
            case "error":
                console[type](param);
                break;

            case "addTime":
            default:
                console.log("%c " + getDateTimeNow() + " %c " + param, "background: gray; color: white;", "");
                break;
        }
    }

    // ------------------------------------------------------------------------------------------ //
    // Getting HTML Elements                                                                      //
    // ------------------------------------------------------------------------------------------ //

    function getMyName() {
        let fullName = document.querySelector('.nav-item__profile-member-photo') ||
                       document.querySelector('.global-nav__me-photo');
        
        fullName = fullName.getAttribute("alt");
        return fullName.slice(0, fullName.indexOf(" "));
    }

    function getNextButton() {
        let nextButton = document.querySelector('button[aria-label="Next"]') ||
                         document.querySelector(".artdeco-pagination__button--next");

        if (nextButton === null ||
            nextButton.getAttribute("disabled") !== null) 
            return null;

        return nextButton;
    }

    function getButtons() {
        return [...document.querySelectorAll(".search-result__actions--primary")].filter(function (element) {
            return element.textContent.trim() === "Connect" ||
                   element.textContent.trim() === "Message"; 
        });
    }

    function getSendInvitationButton() {
        return document.querySelector('button[aria-label="Send invitation"]') ||
               document.querySelector('button[aria-label="Send now"]') ||
               document.querySelector('button[aria-label="Done"]');
    }

    function getSendMessageButton() {
        return document.querySelector('.msg-form__send-button') ||
               document.querySelector('button[data-control-name="send"]');
    }

    function getInvitationTextField() {
        return document.querySelector('textarea[id="custom-message"]') ||
               document.querySelector('.send-invite__custom-message');
    }

    function getMessageTextField() {
        return document.querySelector('.msg-form__contenteditable') ||
               document.querySelector('div[aria-label="Write a message"]');
    }

    function getAddANoteToInvitationButton() {
        return document.querySelector('button[aria-label="Add a note"]'); 
    }

    function getEmailConfirmField() {
        return document.querySelector('input[id="email"]');
    }
    
    function getCloseinvitationBoxButton() {
        return document.querySelector('button[aria-label="Dismiss"]'); 
    }
    
    function getCloseInvitationBoxButtons() {
        return document.querySelectorAll('button[aria-label="Dismiss"]'); 
    }

    function getCloseMessageBoxButton() {
        return document.querySelector('button[data-control-name="overlay.close_conversation_window"]');
    }

    function getCloseMessageBoxButtons() {
        return document.querySelectorAll('button[data-control-name="overlay.close_conversation_window"]');
    }
    
    function getDiscardButton() {
        return document.querySelector('button[class="mlA mr3 artdeco-button artdeco-button--muted artdeco-button--2 artdeco-button--secondary ember-view"]');
    }

    function getLimitReachedGotItButton() {
        return document.querySelector('.ip-fuse-limit-alert__primary-action') || 
               document.querySelector('button[aria-label="Got it"]') || 
               document.querySelector('button[data-control-name="fuse_limit_got_it"]');
    }

    function getInMailStatus() {
        return document.querySelector('.inmail-status');
    }

    function getMessagesList() {
        return document.querySelector('.msg-s-message-list-content');
    }

    function getTimeList() {
        return document.querySelectorAll(".msg-s-message-list__time-heading");
    }

    // ------------------------------------------------------------------------------------------ //
    // Auxiliary logic elements                                                                   //
    // ------------------------------------------------------------------------------------------ //

    function isMessage(HTMLElement) {
        return HTMLElement.getAttribute("aria-label").indexOf(BUTTON_NAME_PRESTRINGS["message"]) !== -1;
    }

    function getPersonNameFromButton(HTMLElement) {
        let strWithName = HTMLElement.getAttribute("aria-label");
        let stringStart = -1;

        for (let element in BUTTON_NAME_PRESTRINGS)
            if (strWithName.indexOf(BUTTON_NAME_PRESTRINGS[element]) !== -1)
                stringStart = BUTTON_NAME_PRESTRINGS[element].length;

        if (stringStart === -1) 
            throw new ScriptError("nameParseError", strWithName);

        return removeNonLatinLetters(strWithName.slice(stringStart, strWithName.indexOf(".")));
    }
    
    function isThisInMailFree() {
        // Just a normal message window (usually for 1st level)
        if (getInMailStatus() === null)
            return true;

        // Can also use .msg-inmail-credits-display__error && "You have no InMail credits left"
        return getInMailStatus().innerText.indexOf(INMAIL_STATUS_STRINGS["freeInMail"]) !== -1;
    }

    function didWeSendSomethingLastMonth() {
        let today = new Date();

        let timeList = getTimeList();
        if (timeList.length === 0) return false;

        let theLastTime = [...timeList][timeList.length].innerText;

        try {
            // All the cases like "Today", "Monday", etc.
            if (theLastTime.indexOf(" ") === -1) return true;

            // "Jun 3", "Jan 15", etc. (this year)
            if (theLastTime.indexOf(", ") === -1) theLastTime = theLastTime + ", " + today.getFullYear();

            // "Jun 3, 2019", "Jan 15, 2020", etc.
            theLastTime = Date.parse(theLastTime);
            return today - theLastTime < INMAIL_DELAY;
        } catch {
            throw new ScriptError("dateParse", theLastTime);
        }
    }

    function wasThisMessageSentBefore() {
        let messagesBox = getMessagesList();
        if (messagesBox === null ||
            messagesBox.innerText.indexOf(INMAIL_ID_LINK) === -1)
            return false;

        return true;
    }

    function getMessageParsed(message, element) {
        NAME = getPersonFirstName(element);

        return message
               // If we can't parse the name properly (it's empty therefore), just saying "Hi" instead
               .replace(NAME ? "{{NAME}}" : " {{NAME}}",
                        NAME ? NAME : "")

               .replace("{{MY_NAME}}", MY_NAME)
               .replace("{{MY_SHORT_NAME}}", MY_SHORT_NAME)
               
               .replace("{{INMAIL_ID_LINK}}", INMAIL_ID_LINK)
               .replace("{{INVITATION_ID_LINK}}", INVITATION_ID_LINK);
    }

    function getPersonFirstName(HTMLelement) {
        let fullName = getPersonNameFromButton(HTMLelement);

        let firstName;
        if (fullName.indexOf(" ") === -1) firstName = fullName;
        else firstName = fullName.slice(0, fullName.indexOf(" "));

        // Getting rid of a bias with initials, mr., dr., and so on
        if (firstName.length <= 2) firstName = "";

        return capitalizeFirstLetter(firstName);
    }

    function cutStringTo300(string) {
        if (string.length > 300)
            // This time it was decided not to cut, but to throw an error
            throw new ScriptError("moreThan300", "[" + string.length + "] " + string);

        return string.slice(0, 300);
    }

    // ------------------------------------------------------------------------------------------ //
    // Auxiliary clean functions                                                                  //
    // ------------------------------------------------------------------------------------------ //

    function capitalizeFirstLetter(str) {
        return str.charAt(0).toUpperCase() + str.toLowerCase().slice(1);
    }

    function removeNonLatinLetters(str) {
        let charset = "a-zA-ZÀÁÂÃÄÅÆÇŒÈÉÊËÌÍÎÏàáâãäåæçœèéêëìíîïÐÑÒÓÔÕÖØÙÚÛÜÝŸÞẞðñòóôõöøùúûüýÿþß";
        let regex = new RegExp(`[^${charset}\\s]`, "g");
        return str.replace(regex, "").trim();
    }

    function formatXX(number) {
        return ("0" + number).slice(-2);
    }

    function getDateTimeNow() {
        let now = new Date();
        
        return formatXX(now.getFullYear()) + "/" +
               formatXX(now.getMonth() + 1) + "/" +
               formatXX(now.getDate()) + " " +
               formatXX(now.getHours()) + ":" +
               formatXX(now.getMinutes()) + ":" +
               formatXX(now.getSeconds());
    }

    function getURIParam(param) {
        param = param.replace(/[\[]/g, '\\[').replace(/[\]]/g, '\\]');
        let regex = new RegExp('[\\?&]' + param + '=([^&#]*)');
        let results = regex.exec(location.search);
        return results === null ? '' 
                                : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    function getRandomDelay(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    function getAfterCLickDelay() {
        return getRandomDelay(MIN_AFTER_CLICK_DELAY, MAX_AFTER_CLICK_DELAY);
    }

    function getAfterPagechangeDelay() {
        return getRandomDelay(MIN_AFTER_PAGECHANGE_DELAY, MAX_AFTER_PAGECHANGE_DELAY);
    }

    function getBunchDelay() {
        return getRandomDelay(MIN_BUNCH_DELAY, MAX_BUNCH_DELAY);
    }

    // ------------------------------------------------------------------------------------------ //
    // Browser actions functions                                                                  //
    // ------------------------------------------------------------------------------------------ //

    async function sleepUntilNextBunch() {
        let interbunchDelay = getBunchDelay();
        await actionWithDelay(() => scriptLog("sleep", 
                                              Math.floor(interbunchDelay/1000)),
                              interbunchDelay);
    }
    
    async function addInvitationMessage(str) {
        let elem = getInvitationTextField();
        await actionWithDelay(() => {
            elem.value = str;

            let theEvent = new Event('input', { 'bubbles': true, 'cancelable': true });
            elem.dispatchEvent(theEvent);
        }, getAfterCLickDelay());
    }
    
    async function addMessageToField(str) {
        let elem = getMessageTextField();
        await actionWithDelay(() => {
            elem.innerHTML = str;

            let theEvent = new Event('input', { 'bubbles': true, 'cancelable': true });
            elem.dispatchEvent(theEvent);
        }, getAfterCLickDelay());
    }

    async function closeInvitationWindow(cross_button) {
        await singleClick(cross_button);
    }

    async function closeAllInvitationWindows() {
        if (getCloseInvitationBoxButtons() === null)
            return;

        for (let elem of [...getCloseInvitationBoxButtons()]) 
            closeInvitationWindow(elem);
    }

    async function closeMessageWindow(cross_button) {
        await singleClick(cross_button);

        if (getDiscardButton())
            await singleClick(getDiscardButton());
    }

    async function closeAllMessageWindows() {
        if (getCloseMessageBoxButtons() === null)
            return;
                                
        for (let elem of [...getCloseMessageBoxButtons()])
            await closeMessageWindow(elem);
    }

    async function scrollAndLoadWholePage() {
        return actionWithDelay(() => window.scrollBy(0, SCROLL_BY),
                               SCROLL_DELAY);
    }

    async function actionWithDelay(action, delay) {
        return new Promise(resolve => {
            action();
            setTimeout(() => resolve(), 
                       delay);
        })
        .catch (() => {
            throw new ScriptError("wrongAction", action);
        });
    }

    async function singleClick(element, after_click_delay = getAfterCLickDelay()) {
        return actionWithDelay(element.click.bind(element), after_click_delay)
    }

    async function goToNextPage() {
        return singleClick(getNextButton(), getAfterPagechangeDelay())    
    }

    // ------------------------------------------------------------------------------------------ //
    // Four primary logic functions                                                               //
    // ------------------------------------------------------------------------------------------ //

    async function invitePerson(connect_button) {                              
        await singleClick(connect_button);

        // Check if we have reached the limit
        if (getLimitReachedGotItButton()) {
            await singleClick(getLimitReachedGotItButton());
            throw new LinkedInError ("invitationsLimit", invitationsSent);
        }

        // Sometimes, connect buttons are not clickable and the modal window just doesn't open
        if (!getSendInvitationButton() ||
            !getCloseinvitationBoxButton())
            throw new ScriptError ("connectButtonDisabled", getPersonNameFromButton(connect_button));

        // If LinkedIn asks email - just close the modal window and continue with the next one
        if (getEmailConfirmField())
            throw new LinkedInError ("emailBlock", getPersonNameFromButton(connect_button));

        // Well, as it is
        if (EMPTY_INVITATION_MODE) {
            await singleClick(getSendInvitationButton());

            emptyInvitationsSent++;
            return scriptLog("emptyInvitationSent", getPersonNameFromButton(connect_button));
        }     

        // If there is a modal window with "add a note" button - click it and make the textarea visible
        if (getAddANoteToInvitationButton())
            await singleClick(getAddANoteToInvitationButton());                                   
        
        // Just expecting any kind of mistakes concerned to textarea value assignment, 'cause they always appear here
        if (getInvitationTextField() === null) 
            throw new ScriptError("textareaInit", getPersonNameFromButton(connect_button));
        
        await addInvitationMessage(cutStringTo300(getMessageParsed(INVITATION_MESSAGE, connect_button)));

        // The check is done on the very beginning;
        await singleClick(getSendInvitationButton());

        invitationsSent++;
        return scriptLog("invitationSent", getPersonNameFromButton(connect_button));
    }

    async function writePerson(message_button) {
        if (NO_MESSAGE_MODE) {
            messagesPassed++
            return scriptLog("messagePassed", getPersonNameFromButton(message_button));
        }

        // Do not send messages to 1st level of connections in this script
        if (isMessage(message_button)) 
            throw new LinkedInError("wrongAdressee", getPersonNameFromButton(message_button));

        // Opening message window
        await singleClick(message_button);

        // Somewhere here should be a limit of sent messages also
        //
        //

        // Sometimes, connect buttons are not clickable and the modal window just doesn't open
        if (!getCloseMessageBoxButton())
            throw new ScriptError("messageButtonDisabled", getPersonNameFromButton(message_button));
        
        // Checking if the message dialogue is blocked (previous message was not accepted)
        if (!getSendMessageButton() ||
            !getMessageTextField())
            throw new LinkedInError("notAccepted", getPersonNameFromButton(message_button));

        // Check if we are using a free version of InMail (important for Premium accounts)
        if (!isThisInMailFree())
            throw new LinkedInError("billedInMail", getPersonNameFromButton(message_button));

        // Check if we sent something, especially the last 30 days
        if (didWeSendSomethingLastMonth())
            throw new LinkedInError("wait30Days", getPersonNameFromButton(message_button));

            // Check whether we had already sent this type of message before
        if (wasThisMessageSentBefore())
            throw new LinkedInError("messageDuplicate", getPersonNameFromButton(message_button));

        await addMessageToField(getMessageParsed(INMAIL_MESSAGE, message_button));

        await singleClick(getSendMessageButton());

        messagesSent++;
        return scriptLog("messageSent", getPersonNameFromButton(message_button));
    }

    async function walkThroughPage() {
        await scrollAndLoadWholePage();
        let buttons = getButtons();
        scriptLog("newPage", buttons.length);

        for (let i = 0; i < buttons.length && actionsCounter < NUMBER_OF_PEOPLE_TO_TRY; i++) {
            try {
                actionsCounter++; 

                if (buttons[i].textContent.trim() === BUTTON_TEXTS["connectButton"])
                    try {
                        await closeAllInvitationWindows();
                        await invitePerson(buttons[i]);
                    } finally {
                        await closeAllInvitationWindows();
                    } 

                else if (buttons[i].textContent.trim() === BUTTON_TEXTS["messageButton"])
                    try {
                        await closeAllMessageWindows();
                        await writePerson(buttons[i]);
                    } finally {
                        await closeAllMessageWindows();
                    }

                else
                    throw new ScriptError("wrongButton", buttons[i].textContent.trim());
            } catch(err) {
                if (err.name !== "ScriptError" &&
                    err.name !== "LinkedInError")
                    throw err;
                
                errorsEncountered++;
                scriptLog("warn", err.name + ". " + err.message);
            }
            
            // Splitting the flow of actions to the bunches of specific size
            if (actionsCounter < NUMBER_OF_PEOPLE_TO_TRY &&
                actionsCounter % BUNCH_SIZE === 0 &&
                // Not the last button on the last page
                (i < buttons.length - 1 || pagesCounter < NUMBER_OF_PAGES_TO_GO_TROUGH && getNextButton()))
                await sleepUntilNextBunch();
        }
    }

    async function init() {
        do {
            pagesCounter++;
            if (pagesCounter > 1 && getNextButton())
                await goToNextPage();

            await walkThroughPage();   

        } while (pagesCounter < NUMBER_OF_PAGES_TO_GO_TROUGH &&
                 actionsCounter < NUMBER_OF_PEOPLE_TO_TRY &&
                 getNextButton());
    }

    // ------------------------------------------------------------------------------------------ //
    // Well, the code of a function                                                               //
    // ------------------------------------------------------------------------------------------ //

    // Wait until the promise from the main function is returned, then clear console and start
    setTimeout(async () => {
        scriptLog("start");

        try {
            await init();
        } catch (err) {
            errorsEncountered++;
            
            if (err.name === "LinkedInError" ||
                err.name === "ScriptError")
                scriptLog("warn", "Uncaught " + err.name + " warning. " + err.message);
            else 
                scriptLog("error", "Uncaught error. " + err.message);
        }
        
        scriptLog("finish");
    }, START_DELAY);
})()