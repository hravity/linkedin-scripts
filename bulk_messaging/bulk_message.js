(function bulkLinkedInMessageFromSearch() {
    "use strict"

    const MAX_NUMBER_OF_ACTIONS         = 200;
    const MAX_NUMBER_OF_PAGES           = 50;
    const NUMBER_OF_ACTIONS_IN_BATCH    = 40; 

    const MY_FULL_NAME                  = ScriptManager.getMyName();
    const MY_FIRST_NAME                 = ScriptManager.getMyFirstName(MY_FULL_NAME);
    const MY_SHORT_NAME                 = ScriptManager.getMyShortName(MY_SHORT_NAME); 
    const MESSAGE_ID_LINK               = "https://why.hravity.com/cat-e";
    const MESSAGES_LIST                 = { initial:
                                              { message: "<p>Hi {{NAME}},</p>" + 
                                                         "<p><br></p>" +
                                                         "<p>I invite you to follow our page on {{ID_LINK}}</p>" +
                                                         "<p><br></p>" +
                                                         "<p>This is a hiring FAQ. Without magic, in simple words and with lots of details, we’ll disclose the most commonplace mistakes, right practices and the roots of HR technology and science around.</p>" +
                                                         "<p><br></p>" +
                                                         "<p>For the last 10 years we’ve been researching and developing a versatile approach to it. And a brief time ago we’ve started sharing our knowledge.</p>" +
                                                         "<p><br></p>" +
                                                         "<p>Message us if there is a question you’d like to get the answer or read about.</p>" +
                                                         "<p><br></p>" +
                                                         "<p>Thank you and welcome :)</p>",
                                                id_link: "https://why.hravity.com/cat-e" },
                                            "afterInvitation"       : "",
                                            "currentMessage"        : ""}

    const MIN_AFTER_PAGECHANGE_DELAY    = 5000;   
    const MAX_AFTER_PAGECHANGE_DELAY    = 15000;

    const MIN_AFTER_CLICK_DELAY         = 1000;
    const MAX_AFTER_CLICK_DELAY         = 3000;

    const MIN_DELAY_BETWEEN_BATCHES     = 3600000;   
    const MAX_DELAY_BETWEEN_BATCHES     = 7200000;

    const START_DELAY                   = 1000; 

    const PAGE_SCROLL_DELAY             = 10000;

    const MESSAGE_SCROLL_DELAY          = 10000;
    const SCROLL_BY                     = 100000; // In pixels 

    const MESSAGE_DELAY                 = 2592000000; // One month in milliseconds.

    const FILTER_PEOPLE_WITH_WORDS      = [ 
                                            // Do not write at all, a person asked directly
                                            "alert", "предупрежд", "bother", "feedback",

                                            // Acquaintances 
                                            "канал", "NaUKMA",

                                            // Someone already agreed and will keep us in backpocket
                                            "remind", "нагадува", "напомина", "promotions", "mind", "pocket", "next"
                                            // Look forward to our next conversation 
                                            // I’ll come back later with promotions and discounts, if you don’t mind

                                            // Have or see something relevant. 
                                          ];

    const PEOPLE_TO_PASS                = [ "Shurick Agapitov" ];

    // ------------------------------------------------------------------------------------------ //
    // Logger, Errors, some text constants and primary variables                                  //
    // ------------------------------------------------------------------------------------------ //

    class ScriptError extends Error {
        constructor(code, param = "") {
            let errors                  = {
                                            // Standard script errors
                                            "cantOpenWindow"        : "Message window to " + param + " can't be opened",

                                            "wrongButton"           : "The action for button " + param + " is not defined",
                                            "textareaInit"          : "Textarea can't be initiated for " + param,
                                            "wrongAction"           : "Action can't be executed: " + param,
                                            "nameParseError"        : "Can't parse a name of a person: " + param,
                                            "dateParse"             : "Can't parse a date: " + param, 
                                        
                                            // Linkedin behaviour based errors
                                            "isInMail"              : "Trying to send an InMail to " + param,
                                            "messagesLimit"         : "Messages limit is reached after " + param + " actions",

                                            "messageDuplicate"      : "This message was sent before to " + param,
                                            "whiteListed"           : "Not sending to white-listed " + param,
                                            "wait30Days"            : "Sent something else to " + param + " withing the last 30 days",

                                            "iDidNotAnswer"         : "",
                                            "noSpamPromise"         : "",
                                            "alreadySentSomething"  : "", // If we need sending something to newbies only (or old contacts)
                                            "weHadALettering"       : "", // Maybe, we should not send anything in this case at all
                                          };

            super(errors[code]);

            this.name = "ScriptError";
            this.errorCode = code;
            this.level = code === "invitationsLimit" ? "SCRIPT"
                       : code === "containerIsAbsent" ? "PAGE"
                       : "ELEMENT";
        }
    }

    let scriptLog = (function() {
        let addNewLineFlag = false;

        function formatXX(number) {
            return ("0" + number).slice(-2);
        }
    
        function getDateTimeNow() {
            let now = new Date();
            
            return formatXX(now.getFullYear()) + "/" +
                   formatXX(now.getMonth() + 1) + "/" +
                   formatXX(now.getDate()) + " " +
                   formatXX(now.getHours()) + ":" +
                   formatXX(now.getMinutes()) + ":" +
                   formatXX(now.getSeconds());
        }

        function getURIParam(param) {
            param = param.replace(/[\[]/g, '\\[').replace(/[\]]/g, '\\]');
            let regex = new RegExp('[\\?&]' + param + '=([^&#]*)');
            let results = regex.exec(location.search);
            return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }

        return function(type, ...params) {
            switch(type) {
                case "start":           scriptLog("clear");
                                        scriptLog("addTime", "So, let's start our workout :)");
                                        break;

                case "newPage":         scriptLog("twoLines", "Starting the page #" + pagesCounter + " [LI: " + (getURIParam("page") || 1) + "] with " + params[0] + " elements");
                                        break;  

                case "sleep":           scriptLog("twoLines", "Going to sleep for " + params[0] + " seconds");
                                        break;

                case "messageSent":     scriptLog("oneLine", actionsCounter + ": A message to a person #" + messagesSent + " is sent - " + params[0]);
                                        break;

                case "finish":          scriptLog("table", { "People interacted" : actionsCounter,
                                                             "Messages sent" : messagesSent,
                                                             "Errors encountered" : errorsEncountered });
                                        break;
                
                case "emptyLine":       scriptLog("log", "");
                                        break;

                case "oneLine":         if (addNewLineFlag) scriptLog("emptyLine");
                                        scriptLog("log", ...params);
                                        addNewLineFlag = false;
                                        break;

                case "addTime":         scriptLog("log", "%c " + getDateTimeNow() + " %c " + params[0], "background: gray; color: white;", "");
                                        break;

                case "twoLines":        scriptLog("emptyLine");
                                        scriptLog("addTime", ...params);
                                        addNewLineFlag = true;
                                        break;

                case "warn":
                case "error":
                case "clear":
                case "table":
                case "log":
                default:                console[type](...params);
                                        break;
            }
        }
    })();

    // ------------------------------------------------------------------------------------------ //
    // Primary classes                                                                            //
    // ------------------------------------------------------------------------------------------ //

    class Lettering {
        static areThereAnyMessages() {
            return getMessagesListContainer() !== null;
        }

        static wasOnlyInvitationSent(msgContainer) {
            if (didWeLoadEverything(msgContainer) === false) return false;

            if (howManyMessages(msgContainer) !== 1 ||
                howManyMessagesDidIsend(msgContainer) !== 1) return false;

            if (getMessageText(getAllMessages(msgContainer)[0]).length > 300) return false;

            if ((new RegExp(`https://go.hravity.com/[a-z]{2}[a-g]`)).exec(getAllMessages(msgContainer)[0]) === null) return false;

            return true;
        }

        getMessageText(msgListItem) {
            return msgListItem.querySelector("msg-s-event-listitem__body").innerText.trim(); 
        }

        // Did we have any kind of conversation. Some messages can be sent only as first.
        static didTheyEverAnswer(msgContainer) {
            return howManyMessages(msgContainer) - howManyMessagesDidIsend(msgContainer) > 0;
        }

        static wasThisMessageSentBefore(msgContainer) {
            if (msgContainer === null ||
                msgContainer.innerText.indexOf(MESSAGE_ID_LINK) === -1)
                return false;
            else
                return true;
        }

        static didWePromiseNotSpam(msgContainer) {
            if (msgContainer === null) return false;

            for (let i = 0; i < FILTER_PEOPLE_WITH_WORDS.length; i++)
                if (msgContainer.innerText.indexOf(FILTER_PEOPLE_WITH_WORDS[i]))
                    return true;

            return false;
        }

        // Do not send anything automatically more often that once a month.
        static didWeSendSomethingLastMonth() {
            return (new Date()) - whenLastSent() < MESSAGE_DELAY;
        }

        // Did we answer their last message, or left it not answered.
        static didIWriteLast(msgContainer) {
            let asd = getAllMessages(msgContainer);
            return [[...asd][asd.length - 1]].filter(filterMyMessages).length == 1;
        }

        static isPersonWhiteListed(nameString) {
            for (let element in PEOPLE_TO_PASS)
                if (nameString.indexOf(element))
                    return true;

            return false;
        }

        /*
         *
         */

        parseMessageDate(dateString) {
            // All the cases like "Today", "Monday", etc.
            if (dateString.indexOf(" ") === -1) {
                let weekDays = [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ];
                let today = new Date();

                // Today string just processed as it is
                if (dateString === "Today") return today;
                
                todayWeekDay = today.getDay();
                messageWeekDay = weekDays.indexOf(dateString);
                let differenceInDays = 7 + todayWeekDay - messageWeekDay;

                return today - differenceInDays * 86400 * 1000;
            }

            // "Jun 3", "Jan 15", etc.,- need to add this year to the string to be parsed.
            if (dateString.indexOf(", ") === -1) dateString = dateString + ", " + (new Date()).getFullYear();

            // "Jun 3, 2019", "Jan 15, 2020", etc.,- are treated as it is.
            return Date.parse(dateString);
        }

        whenFirstSent() {
            let timeList = getTimeList();
            if (timeList.length === 0) return;
            let firstTime = [...timeList][0].innerText;
            return parseMessageDate(firstTime)
        }

        whenLastSent() {
            let timeList = getTimeList();
            if (timeList.length === 0) return;
            let lastTime = [...timeList][timeList.length - 1].innerText;
            return parseMessageDate(lastTime)
        }

        getTimeList(msgContainer) {
            return msgContainer.querySelectorAll(".msg-s-message-list__time-heading");
        }

        /*
         *
         */

        static async scrollMessagesList(msgContainer) {
            for (let i = 0; i < 5; i++) {
                await actionWithDelayAfter(() => msgContainer.scrollTop -= SCROLL_BY, MESSAGE_SCROLL_DELAY);
                if (didWeLoadEverything(msgContainer))
                    return;
            }
        }

        didWeLoadEverything(msgContainer) {
            if (getProfileCard(msgContainer))
                return true;
            else
                return false;
        }

        getProfileCard(msgContainer) {
            return msgContainer.querySelector('.msg-s-profile-card') || 
                   msgContainer.querySelector('.msg-s-profile-card-one-to-one');
        }

        /*
         *
         */

        howManyMessagesDidIsend(msgContainer) {
            return [...getAllMessages(msgContainer)].filter(filterMyMessages).length;
        }

        filterMyMessages(element) {
            return element.querySelector("img[title='" + MY_FULL_NAME + "']") !== null ||
                   element.querySelector("img[alt='" + MY_FULL_NAME + "']") !== null;
        }

        howManyMessages(msgContainer) {
            return getAllMessages(msgContainer).length;
        }

        getAllMessages(msgContainer) {
            return msgContainer.querySelectorAll(".msg-s-message-list__event");
        }

        /*
         *
         */

        getMessagesListContainer() {
            return document.querySelector('.msg-s-message-list');
        }
    }
    
    class MessageBox {
        static async addMessageToField(messageBox, messageArray) {
            await actionWithDelayAfter(() => {
                getMessageTextField(messageBox).innerHTML = getMessageParsed(messageArray["message"], { name: SearchResult.getPersonFirstName(getMessageBoxTitle(messageBox)),
                                                                                                        id_link: messageArray["id_link"] } );
                getMessageTextField(messageBox).dispatchEvent(new Event('input', { 'bubbles': true, 'cancelable': true }));
            });
        }

        isMessageBoxCorrect(messageBox, fullName) {
            return getMessageBoxes().length === 1 &&
                   getMessageBoxTitle(messageBox).innerText.trim() === fullName;
        }

        getMessageBoxTitle(messageBox) {
            return messageBox.querySelector(".msg-overlay-bubble-header__title");
        }

        getPersonFirstName(full_name) {
            let firstName;
            full_name = removeNonLatinLetters();

            if (full_name.indexOf(" ") === -1) firstName = full_name;
            else firstName = full_name.slice(0, full_name.indexOf(" "));
    
            // Getting rid of a bias with initials, mr., dr., and so on
            if (firstName.length <= 2) firstName = "";
    
            return capitalizeFirstLetter(firstName);
        }

        getMessageTextField(messageBox) {
            return messageBox.querySelector('.msg-form__contenteditable') ||
                   messageBox.querySelector('div[aria-label="Write a message"]');
        }

        getMessageParsed(message, params) {    
            return message
                   // If we can't parse the name properly (it's empty therefore), just saying "Hi" instead
                   .replace(params["name"] === "" ? " {{NAME}}" : "{{NAME}}",
                            params["name"] === "" ? ""          : params["name"])
                   .replace("{{ID_LINK}}", params["id_link"]);
        }
        
        /*
         *
         */
        
        static async closeAllMessageWindows() { 
            for (let messageBox of getMessageBoxes())
                await closeMessageWindow(messageBox);
        }

        getMessageBoxMenuButton(messageBox) {
            return messageBox.querySelector(".msg-thread-actions__control");
            // bubbles true cancelable false click
        }

        getMessageBoxMenu(messageBox) {
            return messageBox.querySelector(".artdeco-dropdown__content-inner");
        }

        getMakeUnreadButton(menuBox) {
            return [...menuBox.querySelectorAll(".msg-thread-actions__dropdown-option")]
                   .filter((element) => {return element.innerText.trim() === "Mark as unread"});
        }
        
        getCloseMessageBoxButton(messageBox) {
            return messageBox.querySelectorAll('button[data-control-name="overlay.close_conversation_window"]');
        }

        getMessageBoxTitle_Minimizer(messageBox) {
            return messageBox.querySelector(".msg-overlay-bubble-header") ||
                   messageBox.querySelector(".msg-overlay-conversation-bubble--header");
        }

        async closeMessageWindow(messageBox) {
            // Minimized => make not minimized.
            if (!getMessageBoxMenuButton(messageBox)) getMessageBoxTitle_Minimizer(messageBox).click();

            // Opening menu window and making message unread.
            getMessageBoxMenuButton(messageBox).dispatchEvent(new Event("click", { bubbles: true, cancelable: true }));
            let unreadButton = getMakeUnreadButton(getMessageBoxMenu(messageBox));                                      // Potential error with null
            if (unreadButton) unreadButton.click();

            await singleClick(getCloseMessageBoxButton(messageBox));

            // If there is a need to close a window with a text - so it's better not to answer during the script work.
            if (getDiscardButton()) await singleClick(getDiscardButton());
        }

        static getCorrectMessageBox(fullName) {
            let messageBoxes = [...getMessageBoxes()]
                               .filter((element) => getMessageBoxTitle(element) === fullName)

            if (messageBoxes.length === 0 || messageBoxes.length > 1) return null;
            else return messageBoxes[0];
        }
        
        /*
         *
         */

        getMessageBox() {
            return document.querySelector(".msg-overlay-conversation-bubble");
        }

        getMessageBoxes() {
            return document.querySelectorAll(".msg-overlay-conversation-bubble");
        }

        getSendMessageButton() {
            return document.querySelector('.msg-form__send-button') ||
                   document.querySelector('data-control-name="send"');
        }

        getDiscardButton() {
            return document.querySelector('button[class="mlA mr3 artdeco-button artdeco-button--muted artdeco-button--2 artdeco-button--secondary ember-view"]');
        }
    }

    class SearchResult {
        static getPersonFullNameString(LIElement) {
            return LIElement.querySelector(".actor-name").textContent.trim();
        }
    
        removeNonLatinLetters(str) {
            let charset = "a-zA-Z" + 
                          "0-9" +
                          "ÀÁÂÃÄÅÆÇŒÈÉÊËÌÍÎÏàáâãäåæçœèéêëìíîïÐÑÒÓÔÕÖØÙÚÛÜÝŸÞẞðñòóôõöøùúûüýÿþß" + 
                          // For double names and surnames.
                          "-";
            let regex = new RegExp(`[^${charset}\\s]`, "g");
            return str.replace(regex, "").trim();
        }

        capitalizeFirstLetter(str) {
            if (str.toUpperCase() === str ||
                str.toLowerCase() === str)
                return str.charAt(0).toUpperCase() + str.toLowerCase().slice(1);
                
            return str;
        }

        static getMessageButton(listElement) {
            return listElement.querySelector(".search-result__actions--primary"); 
        }
    }

    class SearchPage {
        static async scrollAndLoadWholePage() {
            return actionWithDelayAfter(() => window.scrollBy(0, SCROLL_BY), PAGE_SCROLL_DELAY);
        }

        static getSearchResultsFilteredList() {
            return getSearchResultsList().filter(shouldWeAct);
        }
        
        getSearchResultsList() {
            return [...getSearchResultsContainer().querySelectorAll(".search-result__occluded-item")];
        }

        getSearchResultsContainer = function() {
            document.querySelector(".search-results__list");
        }

        shouldWeAct(listElement) {
            let buttonElement = SearchResult.getMessageButton(listElement
                )
            if (buttonElement.value.textContent.trim() !== "Message" ||
                buttonElement.getAttribute("disabled") !== null ||
                // There is no lock icon on the button. No need here.
                // HTMLElement.querySelector("li-icon[type='lock-icon']") !== null ||
                isInMail(buttonElement)) 
                return false;

            return true;
        }

        isInMail(HTMLElement) {
            if (HTMLElement.getAttribute("aria-label").indexOf("Send InMail to ") === 0) 
                return true;
            
            // "Send message to " usually
            return false;
        }
    }

    class ScriptManager {
        static pagesCounter = 0;
        static actionsCounter = 0;
    
        static messagesSent = 0;
        static errorsEncountered = 0;

        /*
         *
         */

        static async goToNextPage() {
            if (shouldWeGoToNextPage())
                return singleClick(getNextButton(), 
                                   getRandomDelay(MIN_AFTER_PAGECHANGE_DELAY,
                                                  MAX_AFTER_PAGECHANGE_DELAY));
        }

        static shouldWeGoToNextPage() {
            if (actionsCounter >= MAX_NUMBER_OF_ACTIONS) return false;
            if (isLastPage()) return false;
    
            return true;
        }

        static isLastPage() {
            if (pagesCounter >= MAX_NUMBER_OF_PAGES) return true;
            if (getNextButton() === null) return true;
    
            return false;
        }

        getNextButton() {
            let nextButton = document.querySelector('button[aria-label="Next"]') ||
                             document.querySelector(".artdeco-pagination__button--next");

            if (nextButton === null ||
                nextButton.getAttribute("disabled") !== null) 
                return null;

            return nextButton;
        }
        
        static async sleepUntilNextBatch() {
            let interBatchDelay = getRandomDelay(MIN_DELAY_BETWEEN_BATCHES, 
                                                 MAX_DELAY_BETWEEN_BATCHES);
            await actionWithDelayAfter(() => scriptLog("sleep", 
                                                       Math.floor(interBatchDelay/1000)),
                                       interBatchDelay);
        }

        static async singleClick(element, ...params) {
            return actionWithDelayAfter(element.click.bind(element), ...params)
        }

        static async actionWithDelayAfter(action, 
                                          delay = getRandomDelay(MIN_AFTER_CLICK_DELAY,
                                                                 MAX_AFTER_CLICK_DELAY)) {
            return new Promise(resolve => { action();
                                            setTimeout(() => resolve(), delay); })
                       .catch (() => { throw new ScriptError("wrongAction", action); });
        }
        
        getRandomDelay(min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }

        getMyName() { 
            return (document.querySelector('.nav-item__profile-member-photo') ||
                    document.querySelector('.global-nav__me-photo'))
                   .getAttribute("alt"); 
        }

        getMyFirstName(fullName) {
            return fullName.replace(/ .*/i, "");
        }

        getMyShortName(firstName) {
            return ( { Valdemar               : "Valdo",
                       Zoriana                : "Zori",
                       Natali                 : "Nata",
                       Anastasia              : "Ana",
                       Taline                 : "Tali" } ) [firstName] ||
                    firstName; 
        }
    }

    // ------------------------------------------------------------------------------------------ //
    // Three primary logic functions                                                              //
    // ------------------------------------------------------------------------------------------ //

    async function writePerson(listElement) {
        try {
            let personFullNameFromButton = SearchResult.getPersonFullNameString(listElement);

            // We should avoid speaking with some people.
            if (Lettering.isPersonWhiteListed(personFullNameFromButton)) throw new ScriptError("whiteListed", personFullNameFromButton);

            let correctMessageBox = null;
            let messageBoxPointAttempts = 5;

            // Only one window should be open to work correctly, however they can open in any moment by the others.
            do { await MessageBox.closeAllMessageWindows();
                 await ScriptManager.singleClick(SearchResult.getMessageButton(listElement));

                 messageBoxPointAttempts--;
                 correctMessageBox = MessageBox.getCorrectMessageBox(personFullNameFromButton);
            } while (correctMessageBox === null &&
                     messageBoxPointAttempts > 0)

            // The window is not open still or two windows with similar name are open somehow or due to LinkedIn bug.
            if (correctMessageBox === null) throw new ScriptError("cantOpenWindow", personFullNameFromButton);

            // What is the reason to write to people who had already answered? They already know us.
            if (Lettering.didTheyEverAnswer(correctMessageBox)) throw new ScriptError("hadConversationAlready", personFullNameFromButton)

            // Check if we sent something the last 30 days
            if (Lettering.didWeSendSomethingLastMonth(correctMessageBox)) throw new ScriptError("wait30Days", personFullNameFromButton);

            message: {
                if (Lettering.areThereAnyMessages(correctMessageBox) === false) {
                    await MessageBox.addMessageToField(correctMessageBox), MESSAGES_LIST["initial"];
                    break message;
                }

                if (Lettering.wasOnlyInvitationSent(correctMessageBox)) {
                    await MessageBox.addMessageToField(correctMessageBox), MESSAGES_LIST["afterInvitation"];
                    break message;
                }

                // if there were standard description message (who we are and what we do, what our strength) => ...
                // we need a story in messages
            }

            await singleClick(MessageBox.getSendMessageButton(correctMessageBox));

            // Somewhere here should be a limit of sent messages
            //
            //

            // Somewhere here should be something like messages throttling
            //
            //

            ScriptManager.messagesSent++;
            scriptLog("messageSent", personFullNameFromButton);
        } catch(err) {
            if (err.name !== "ScriptError" || err.level !== "ELEMENT") throw err;
            ScriptManager.errorsEncountered++;
            scriptLog("warn", err.name + " [ELEMENT LEVEL]: " + err.message);
        } finally {
            await MessageBox.closeAllMessageWindows();
        }
    }

    async function walkThroughPage() {
        try {
            ScriptManager.pagesCounter++;

            await SearchPage.scrollAndLoadWholePage();
            let elements = SearchPage.getSearchResultsFilteredList();

            scriptLog("newPage", elements.length);

            for (let i = 0; i < elements.length && ScriptManager.actionsCounter < MAX_NUMBER_OF_ACTIONS; ++i) {
                ScriptManager.actionsCounter++;

                await writePerson(elements[i]);
                
                if (ScriptManager.actionsCounter % NUMBER_OF_ACTIONS_IN_BATCH !== 0 ||
                    ScriptManager.actionsCounter >= MAX_NUMBER_OF_ACTIONS ||
                    ScriptManager.isLastPage() && i >= elements.length - 1) 
                    continue;

                await ScriptManager.sleepUntilNextBatch();
            }
        } catch(err) {
            if (err.name !== "ScriptError" || err.level !== "PAGE") throw err;
            ScriptManager.errorsEncountered++;
            scriptLog("warn", err.name + " [PAGE LEVEL]:  " + err.message);
        }
    }

    async function init() {
        do {
            await walkThroughPage();
            await ScriptManager.goToNextPage();
        } while (ScriptManager.shouldWeGoToNextPage());
    }
    
    // ------------------------------------------------------------------------------------------ //
    // Well, the code of a function                                                               //
    // ------------------------------------------------------------------------------------------ //

    // Initializing the function asynchrounously
    return new Promise(resolve => setTimeout(() => resolve(), 
                                             START_DELAY))
           .then(() => scriptLog("start"))
           .then(() => init())
           .catch((err) => { ScriptManager.errorsEncountered++;
                             scriptLog("error", err.name + " [SCRIPT LEVEL]: " + err.message); })
           .then(() => scriptLog("finish"));
})();

// Add sequences of messages to different types of people (ukrainian CEO, international CEO, venture investors, HR specialists).
// Message field is not secured of other windows opening.
// Make message not read (in order not to lose it further) - those the other opened
// What can we write to all who answered once?

// Shurik Agapitov & other people (blogger, and so on)
// Search page halt
// + .then <=> catch 


// People, whose only reaction was a like or smth like that.