(async function report_stats() {
    const MAX_SCROLL_ITERATIONS         = 20;
    const SCROLL_DELAY                  = 3000;
    const SCROLL_BY                     = 100000; // In pixels 

    const IDS                           = [ "Shchudlyk Irene",
                                            "Balas Alex",
                                            "Zhuravel Nady",
                                            "Sydorchuk Anastasia",
                                            "Ostapenko Natali",
                                            "Boozhik Taline",
                                            "Yavorska Sofia",

                                            "Ohotuk Waldemar",
                                            "Okhotyuk Volodymyr",
                                            "Asanenko Bohdan",
                                            "Vlasenko Mary",
                                            "Filatova Lesia",
                                            "Lintovska Zoriana",
                                            "Rivkin Nick" ];

    const DATE_LETTERS                  = { Mon: "a",
                                            Tue: "b",
                                            Wed: "c",
                                            Thu: "d",
                                            Fri: "e",
                                            Sat: "f",
                                            Sun: "g" };

    const REPORT                        = {};
    let MESSAGE                         = "";

    // -------------------------------------------------------------------------------------------------------------- //

    function getListElements() {
        return document.querySelectorAll(".ListRow--full");
    }

    function getLinksCounterContainer() {
        return document.querySelector(".LinksToolbar__counter");
    }

    function getLinkContainer(element) {
        return element.querySelector(".LinkRow__destination");
    }

    function getDataContainer(element) {
        return element.querySelector("p[class='Text--SubDetail Text--small']");
    }

    // -------------------------------------------------------------------------------------------------------------- //

    function filterListElements(whole_list) {
        let regularka = new RegExp("^go.hravity.com/[a-z]{3}$");

        return [...whole_list].filter(function (element) {
            return regularka.test(getLinkContainer(element).innerText);
        });
    }

    function getInitials(full_name) {
        return full_name.charAt(0).toLowerCase() + full_name.charAt(full_name.indexOf(" ") + 1).toLowerCase();
    }

    function getNameFromLink(rebrandly_link) {
        // Be sure there is no https://
        // Hardcoded according to go.hravity.com/
        let initials = rebrandly_link.charAt(15) + rebrandly_link.charAt(16);

        // It is better to have an additional array with initials, I guess :)
        for (let fullname of IDS)
            if (getInitials(fullname) === initials) 
                return fullname;

        throw "Name can't be determined" + rebrandly_link;
    }

    function getDayFromLink(rebrandly_link) {
        let day = rebrandly_link.charAt(17);
        for (i in DATE_LETTERS)
            if (DATE_LETTERS[i] === day)
                return i;

        throw "Yakas hernia z datamy" + rebrandly_link;
    }

    // -------------------------------------------------------------------------------------------------------------- //

    async function actionWithDelay(action, delay = getAfterClickDelay()) {
        return new Promise(resolve => { action();
                                        setTimeout(() => resolve(), delay); })
                   .catch (() => { throw "What kind of shit did you ask me to make postponed?"; });
    }

    async function loadWholePage() {
        let numberOfLinks = parseInt(getLinksCounterContainer().innerText.trim());

        for (let i = 0; i < MAX_SCROLL_ITERATIONS; i++)
            if (getListElements().length < numberOfLinks) {
                await actionWithDelay(() => window.scrollBy(0, SCROLL_BY), SCROLL_DELAY);
            }
            else 
                return;
    }

    function buildReportStructure() {
        for (let fullname of IDS) {
            REPORT[fullname] = {};
            for (let weekday in DATE_LETTERS)
                REPORT[fullname][weekday] = "";
        }

        return REPORT;
    }

    function buildSlackMessage() {
        let str = []
        for (let fullname of IDS)
            str.push("*" + fullname + ":* " + Object.values(REPORT[fullname]).join(", "));

        return str.join("\n");
    }

    function parseData() {
        let listElements = filterListElements(getListElements());

        for (elem of listElements) {
            let linkText = getLinkContainer(elem).innerText.trim();
            REPORT[getNameFromLink(linkText)][getDayFromLink(linkText)] = parseInt(getDataContainer(elem).innerText.trim());
        }
    }

    function formatXX(number) {
        return ("0" + number).slice(-2);
    }


    async function sendSlackMessage(text) {
        let address = "https://slack.com/api/chat.postMessage" 
                    + "?token=xoxb-19188155909-1321702106465-qarYOHyaHagQTK19bAemNOE4"
                    + "&channel=C019A1UPVBN"
                    + '&text=' + encodeURI(text);
        // console.log(address);

        return new Promise((resolve, reject) => { let channel = new XMLHttpRequest();
                                                  channel.open("GET", address);
                                                  channel.send("");
                                                  channel.onerror = () => {reject()};
                                                  channel.onload = () => resolve(); });
    }

    await loadWholePage();
    buildReportStructure();
    parseData();
    console.table(REPORT);
    //console.log(buildSlackMessage());
    await sendSlackMessage("*[ Clicks Stats for " + formatXX((new Date).getFullYear()) + "-" + formatXX((new Date).getMonth()) + "-" + formatXX((new Date).getDate()) + " ]*" + "\n\n" +
                           buildSlackMessage());
})();