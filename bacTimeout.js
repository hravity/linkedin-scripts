(async function testTimeout(delay, iterations) {
    async function bacTimeout(func, delay, ...params) {

        // Creating a worker by passing a function to it.
        function createWorker(func) {
            let blobURL = URL.createObjectURL(new Blob([ '(',
                                                         func,
                                                         ')()' ], 
                                              { type: 'application/javascript' } ));
            let worker = new Worker(blobURL);
            // URL.revokeObjectURL(blobURL);
            return worker;
        }

        // Creating a worker, which will manage a timeout.
        function wrkTimeout(delay) {
            let worker = createWorker(function() { setTimeout(() => postMessage("EOW"), delay) }.toString().replace("delay", delay));

            /*
            let worker = createWorker( (() => { let functionString = function(){ setTimeout(() => postMessage("EOW"), delay) }.toString().replace("delay", delay);
                                                let resultFunction;
                                                try { resultFunction = eval("(" + functionString + ")"); }
                                                catch(err) { throw new ScriptError("securityPolicy"); }
                                                return resultFunction; })() );
            */

            return new Promise(resolve => { 
                worker.onmessage = (msg) => {
                    worker.terminate();
                    resolve();
                }
            });
        }

        // Creating a standard timer.
        function oldTimeout(delay) {
            return new Promise( resolve =>
                setTimeout( () => resolve(), delay )
            );
        }

        let olschoolTimeout = oldTimeout(delay);
        let workerTimeout   = wrkTimeout(delay);

        return new Promise(resolve => {
            let mutexed = false;
            let fu = () => { if (mutexed === true) return;
                             mutexed = true;
                             func(...params); 
                             resolve(); }

            let fu_test = (actor) => { winners[winners.length - 1].push(actor, (performance.now() - testStart));
                                       if (mutexed === true) resolve();
                                       mutexed = true;
                                       return; }

            workerTimeout.then( () => fu_test("Worker") ); 
            olschoolTimeout.then( () => fu_test("Oldschool") );
        });
    }

    // To lose focus
    window.open("https://slack.com");

    let winners = [];
    let testStart;

    for (let i = 0; i < iterations; i++) {
        winners.push([]);
        testStart = performance.now();
        await bacTimeout(() => {}, delay);
        console.log(winners[winners.length - 1]);
    }

    let oldschoolWinners = winners.filter((element) => element[0] === "Oldschool");
    let workerWinners = winners.filter((element) => element[0] === "Worker");
    let win_ers = oldschoolWinners.length >= workerWinners.length
                ? oldschoolWinners
                : workerWinners;

    console.log("All have " + (winners.reduce((sum, current) => sum + current[1], 0) / winners.length) + " average result and " + winners.map(element => element[1].toFixed(2)).sort((a, b) => b - a).slice(0, 10).join(", ") + " max elements");

    let oldschools = winners.map(element => { if (element[0] === "Oldschool") return [element[0], element[1]]; else return [element[2], element[3]]; });
    console.log("Oldschools have " + (oldschools.reduce((sum, current) => sum + current[1], 0) / oldschools.length).toFixed(2) + " average and " + oldschools.map(element => element[1].toFixed(2)).sort((a, b) => b - a).slice(0, 10).join(", ") + " max elements");

    let workers = winners.map(element => { if (element[0] === "Worker") return [element[0], element[1]]; else return [element[2], element[3]]; });
    console.log("Workers have " + (workers.reduce((sum, current) => sum + current[1], 0) / workers.length).toFixed(2) + " average and " + workers.map(element => element[1].toFixed(2)).sort((a, b) => b - a).slice(0, 10).join(", ") + " max elements");

    console.log("The winner is " + win_ers[0][0] + " with " + win_ers.length + " results of " + iterations);

    // To focus the window
    window.open("https://slack.com");

})(0, 100);

// + Remove eval
// Make the test running from the outside only
// Make a memory test
// Make cleartimeout
// https://ru.stackoverflow.com/questions/912911/Работа-settimeout-в-фоновом-режиме -- post it further