function getSearchBox() {
    return document.querySelector(".groups-members-list__search").querySelector("input[aria-label='Search members']");
}


function triggerInputEvent(elem, text) {
    elem.value = text;
    let ev = new Event("input", {bubbles: true, cancelable: true})
    elem.dispatchEvent(ev)
}


let elem = getSearchBox();
monitorEvents(elem);
triggerInputEvent(elem, "raid")
