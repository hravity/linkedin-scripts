(async () => {
    "use strict"
       
    const WITHDRAW_AGE                  = "week";
    const INVITATIONS_TO_WITHDRAW       = 1300;
    const PAGES_TO_KEEP                 = 3;

    const MIN_AFTER_CLICK_DELAY         = 500;
    const MAX_AFTER_CLICK_DELAY         = 1000;

    const MIN_AFTER_PAGECHANGE_DELAY    = 3000;   
    const MAX_AFTER_PAGECHANGE_DELAY    = 5000;

    const SCROLL_DELAY                  = 5000;
    const SCROLL_BY                  	= 100000; // In pixels
    
    const START_DELAY                   = 1000
    
    /*
     *
     */

    let pagesCounter                    = 0;
    let actionsCounter                  = 0;
  
  	let noMoreInvitationsToWithdraw     = false;

    let invitationsWithdrawn            = 0;
    let errorsEncountered               = 0;

    // ------------------------------------------------------------------------------------------ //
    // Logger, Errors, some text constants and primary variables                                  //
    // ------------------------------------------------------------------------------------------ //

    class ScriptError extends Error {
        constructor(code, param) {
            let errors = {
                "wrongAction"           : "Action can't be executed: {{PARAM}}",
                "windowDisabled"        : "Can't open withdraw window to {{PARAM}}",
            }

            let message = errors[code].replace("{{PARAM}}", param);
            super(message);
            this.name = "ScriptError";
        }
    }

    let scriptLog = (function() {
        let addNewLineFlag = false;

        function formatXX(number) {
            return ("0" + number).slice(-2);
        }
    
        function getDateTimeNow() {
            let now = new Date();
            
            return formatXX(now.getFullYear()) + "/" +
                   formatXX(now.getMonth() + 1) + "/" +
                   formatXX(now.getDate()) + " " +
                   formatXX(now.getHours()) + ":" +
                   formatXX(now.getMinutes()) + ":" +
                   formatXX(now.getSeconds());
        }

        return function(type, ...params) {
            switch(type) {
                case "start":           scriptLog("clear");
                                        scriptLog("addTime", "So, let's start our workout :)");
                                        break;

                case "firstPage":       scriptLog("twoLines", "Passing the first page");
                                        break;

                case "newPage":         scriptLog("twoLines", "Starting the page #" + pagesCounter + " [LI: " + (getURIParam("page") || 1) + "] with " + params[0] + " elements");
                                        break;  

                case "withdrawal":      scriptLog("oneLine", actionsCounter + ": Invitation #" + invitationsWithdrawn + " is withdrawn from " + params[0]);
                                        break;

                case "finish":          scriptLog("table", { "People interacted" : actionsCounter,
                                                             "Invitations withdrawn": invitationsWithdrawn,
                                                             "Errors encountered" : errorsEncountered });
                                        break;
                
                case "emptyLine":       scriptLog("log", "");
                                        break;

                case "oneLine":         if (addNewLineFlag) scriptLog("emptyLine");
                                        scriptLog("log", ...params);
                                        addNewLineFlag = false;
                                        break;

                case "addTime":         scriptLog("log", "%c " + getDateTimeNow() + " %c " + params[0], "background: gray; color: white;", "");
                                        break;

                case "twoLines":        scriptLog("emptyLine");
                                        scriptLog("addTime", ...params);
                                        addNewLineFlag = true;
                                        break;

                case "warn":
                case "error":
                case "clear":
                case "table":
                case "log":
                default:                console[type](...params);
                                        break;
            }
        }
    })();

    // ------------------------------------------------------------------------------------------ //
    // Getting HTML Elements                                                                      //
    // ------------------------------------------------------------------------------------------ //

    function getOuterWithdrawButton(HTMLElement) {
        return HTMLElement.querySelector("button[data-control-name='withdraw_single']");
    }

    function getDateElement(LIElement) {
        return LIElement.querySelector(".time-badge") || 
               LIElement.querySelector(".time-ago");
    }

    function getInvitationCards() {
        return document.querySelectorAll(".invitation-card") || 
               document.querySelectorAll(".invitation-card--selectable");
    }
  
    function getNextButton() {
        let nextButton = document.querySelector('button[aria-label="Next"]') ||
                         document.querySelector(".artdeco-pagination__button--next");

        if (nextButton === null ||
            nextButton.getAttribute("disabled") !== null) 
            return null;

        return nextButton;
    }
  
    function getPrevButton() {
        let nextButton = document.querySelector('button[aria-label="Previous"]') ||
                         document.querySelector(".artdeco-pagination__button--previous");

        if (nextButton === null ||
            nextButton.getAttribute("disabled") !== null) 
            return null;

        return nextButton;
    }

    function getPageButtons() {
        return document.querySelectorAll(".artdeco-pagination__indicator--number");
    }

    function getLastPage() {
        let pageButtons = [...getPageButtons()];
        let lastElement = pageButtons[pageButtons.length - 1];

        return lastElement.querySelector("button");
    }

    function getSecondLastPage() {
        let pageButtons = [...getPageButtons()];
        let lastElement = pageButtons[pageButtons.length - 2];

        return lastElement.querySelector("button");
    }

    function getCloseCross() {
        return document.querySelector('button[aria-label="Dismiss"]') ||
               document.querySelector('artdeco-modal__dismiss');
    }

    // Buttons `cancel` and `withdraw` have similar credentials. 
    // Just for fun it was decided to use this way of getting it.
    function getWithdrawButton() {
        return [...document.querySelectorAll(".artdeco-modal__confirm-dialog-btn")]
               .filter(function (element) {
                   // return element.textContent.trim() === "Cancel"; 
                   return element.getAttribute("data-test-dialog-primary-btn") !== null;
               })[0];
    }

    function getPageWithdrawArray() {
        return [...getInvitationCards()].filter(filterElementsByDate);
    }

    function getPersonName(LIelement) {
        return LIelement.querySelector(".invitation-card__title").innerText.trim();
    }
  
    // ------------------------------------------------------------------------------------------ //
    // Auxiliary logic elements                                                                   //
    // ------------------------------------------------------------------------------------------ //

    function getURIParam(param) {
        param = param.replace(/[\[]/g, '\\[').replace(/[\]]/g, '\\]');
        let regex = new RegExp('[\\?&]' + param + '=([^&#]*)');
        let results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    function getLIPage() {
        return getURIParam("page") || "1";
    }

    function filterElementsByDate(LIElement) {
        let timeArray = ["second", "minute", "day", "week", "month", "year"];
        let timeString = getDateElement(LIElement).innerText.trim();
        let invitationDelayIndex = timeArray.indexOf(WITHDRAW_AGE);
        
        for (let elementIndex = invitationDelayIndex; elementIndex < timeArray.length; elementIndex++)
            if (timeString.indexOf(timeArray[elementIndex]) !== -1 ||
                timeString.indexOf(timeArray[elementIndex] + "s") !== -1)
                return true;

        return false;
    }

    function getRandomDelay(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
  
    function getAfterClickDelay() {
        return getRandomDelay(MIN_AFTER_CLICK_DELAY, MAX_AFTER_CLICK_DELAY);
    }

    function getAfterPagechangeDelay() {
        return getRandomDelay(MIN_AFTER_PAGECHANGE_DELAY, MAX_AFTER_PAGECHANGE_DELAY);
    }
  
    async function actionWithDelay(action, delay = getAfterClickDelay()) {
        return new Promise(resolve => {
            action();
            setTimeout(() => resolve(), 
                       delay);
        })
        .catch (() => {
            throw new ScriptError("wrongAction", action);
        });
    }

    async function singleClick(element, ...params) {
        return actionWithDelay(element.click.bind(element), ...params)
    }
  
    async function goToLastPage() {
        if (getNextButton()) {
            await singleClick(getLastPage(), getAfterPagechangeDelay());
            if (getLIPage() === "1" && getPageButtons().length > 2)
                // Sometimes LI lags with going to the last page after deleting previous one
                // Draws like 701 people instead of 700 and creates additional page in pager, non-working however
                await singleClick(getSecondLastPage(), getAfterPagechangeDelay());
            return
        }

        // We are already on the last page.
        if (getPrevButton()) return;

        // There is only one page left.
        noMoreInvitationsToWithdraw = true;
    }
  
  	async function waitFirstPage() {
        // Can be started anywhere, this line should be only if started with the first page.
        if (getPrevButton() === null || pagesCounter == 0) scriptLog("firstPage", "");

        // Waiting while the page reloads by itself (LI logic after deleting all invitations on the page)
        if (pagesCounter > 0) await actionWithDelay(() => {}, getAfterPagechangeDelay());

        await scrollAndLoadWholePage();
    }
  
    async function scrollAndLoadWholePage() {
        return actionWithDelay(() => window.scrollBy(0, SCROLL_BY), SCROLL_DELAY);
    }
  
    // ------------------------------------------------------------------------------------------ //
    // Auxiliary logic elements                                                                   //
    // ------------------------------------------------------------------------------------------ //

    async function withdrawInvitation(LIElement) {
        actionsCounter++;

        // Opening message window
        await singleClick(getOuterWithdrawButton(LIElement));

        // Sometimes, connect buttons are not clickable and the modal window just doesn't open
        if (!getCloseCross() ||
            !getWithdrawButton()) {
            throw new ScriptError("windowDisabled", getPersonName(LIElement));
        }

        await singleClick(getWithdrawButton());
        invitationsWithdrawn++;
        scriptLog("withdrawal", getPersonName(LIElement));
    }
  
  	async function walkThroughPage() {
        pagesCounter++;
          
      	await scrollAndLoadWholePage();
      
      	let withdrawArray = getPageWithdrawArray();

        if (withdrawArray.length <= 0 || 
            getLIPage() <= PAGES_TO_KEEP) { noMoreInvitationsToWithdraw = true;
          	                                return; }

      	scriptLog("newPage", withdrawArray.length);
      
        for (let i = withdrawArray.length - 1; i >= 0; i--) {
            if (invitationsWithdrawn >= INVITATIONS_TO_WITHDRAW) { noMoreInvitationsToWithdraw = true;   
                                                                   return; }
            try { await withdrawInvitation(withdrawArray[i]); }
            catch (err) { if (err.name !== "ScriptError") throw err;
                          errorsEncountered++;
                          scriptLog("warn", err.name + ". " + err.message); }
        }
  	}

    async function init() {
        do {
            await waitFirstPage();   
            await goToLastPage();
          	await walkThroughPage();
        } while (!noMoreInvitationsToWithdraw);
    }
  
    // ------------------------------------------------------------------------------------------ //
    // Well, the code of a function                                                               //
    // ------------------------------------------------------------------------------------------ //

    // Initializing the function asynchrounously
    return new Promise(resolve => setTimeout(() => resolve(), 
                                             START_DELAY))
           .then(() => scriptLog("start"))
           .then(() => init())
           .catch((err) => { errorsEncountered++;
                             scriptLog("error", err.name + ": " + err.message); })
           .then(() => scriptLog("finish"));
})()

// How to identify people who were removed before and then again appeared in withdraw